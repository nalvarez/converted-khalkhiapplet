/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef ADDRESSEELISTSPAN_H
#define ADDRESSEELISTSPAN_H


// kde specific
#include <kabc/addressee.h>
#include <kabc/addresseelist.h>


struct AddresseeListSpan {
public:
    AddresseeListSpan( KABC::Addressee::List::ConstIterator &B,
                       int NumberLimit, KABC::Addressee::List::ConstIterator ItLimit );
    AddresseeListSpan( const KABC::AddresseeList List );
    AddresseeListSpan( const AddresseeListSpan &Original );
public:
    KABC::Addressee::List::ConstIterator begin() const;
    KABC::Addressee::List::ConstIterator end()   const;
    int number()                                 const;
    KABC::Addressee first() const;
    KABC::Addressee last() const;
protected:
    KABC::Addressee::List::ConstIterator Begin;
    KABC::Addressee::List::ConstIterator End;
    int Number;
};

inline AddresseeListSpan::AddresseeListSpan( KABC::Addressee::List::ConstIterator &B,
                                             int NumberLimit, KABC::Addressee::List::ConstIterator ItLimit )
: Begin( B ), End( B ), Number( 0 )
{
    while( Number < NumberLimit && End != ItLimit )
    { ++End; ++Number; }
    B = End;
}

inline AddresseeListSpan::AddresseeListSpan( const KABC::AddresseeList List )
: Begin( List.begin() ), End( List.end() ), Number( List.count() )
{}

inline AddresseeListSpan::AddresseeListSpan( const AddresseeListSpan &Original )
: Begin( Original.begin() ), End( Original.end() ), Number( Original.number() )
{}

inline KABC::Addressee::List::ConstIterator AddresseeListSpan::begin() const { return Begin; }
inline KABC::Addressee::List::ConstIterator AddresseeListSpan::end()   const { return End; }
inline int AddresseeListSpan::number()                                 const { return Number; }
inline KABC::Addressee AddresseeListSpan::first() const { return *Begin; }
inline KABC::Addressee AddresseeListSpan::last()  const { KABC::Addressee::List::ConstIterator L = End; return *--L; }

#endif
