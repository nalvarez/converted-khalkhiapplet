/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PERSONLISTMENUBUTTONFILLER_H
#define PERSONLISTMENUBUTTONFILLER_H


// qt specific
#include <qstring.h>
// kde specific
#include <kabc/addressee.h>
#include <kabc/addresseelist.h>
// lib specific
#include "menubuttonfiller.h"
// applet
#include "personlistmenufiller.h"


class PersonListMenuButtonFiller : public MenuButtonFiller
{
public:
    PersonListMenuButtonFiller( const KABC::AddresseeList &List, const QString &IconName,
                                 KNameType NameType, const QString &T, bool IC );
    virtual ~PersonListMenuButtonFiller();

public: // MenuButtonFiller API
    // fills a menu button with pixmaps, tooltip and popupmenu
    virtual void fill( MenuButton *Button );
    // fills a menu button with pixmaps
    virtual void fillIcon( MenuButton *Button );

public:
    void setNameType( KNameType NewNameType );
    void setIconName( const QString &IconName );
    void setSortField( KABC::Field * SortField );
    void setSortOrder( Qt::SortOrder SortOrder );
    void setGroupServices( bool GroupServices );

public:
    bool isCategory() const;
    QString titel() const;
    QString iconName() const;
    KNameType nameType() const;
    KABC::Field *sortField() const;
    Qt::SortOrder sortOrder() const;
    bool groupServices() const;
    KABC::AddresseeList personList() const;

protected:
    KABC::AddresseeList PersonList;
    QString IconName;
    KNameType NameType;
    KABC::Field *SortField;
    Qt::SortOrder SortOrder;
    bool GroupServices;
    QString Titel;
    const bool IsCategory;
};

inline bool PersonListMenuButtonFiller::isCategory()    const { return IsCategory; }
inline QString PersonListMenuButtonFiller::titel()      const { return Titel; }
inline QString PersonListMenuButtonFiller::iconName()   const { return IconName; }
inline KNameType PersonListMenuButtonFiller::nameType() const { return NameType; }
inline KABC::Field *PersonListMenuButtonFiller::sortField()  const { return SortField; }
inline Qt::SortOrder PersonListMenuButtonFiller::sortOrder() const { return SortOrder; }
inline bool PersonListMenuButtonFiller::groupServices()       const { return GroupServices; }

#endif
