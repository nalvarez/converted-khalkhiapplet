/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// menu specific
#include "menufiller.h"
#include "lazyfillmenu.h"


LazyFillMenu::LazyFillMenu( MenuFiller *F, bool R, QWidget *Parent, const char *Name )
  : QPopupMenu( Parent, Name ), Filler( F ), Refill( R ), Empty( true )
{
    connect( this, SIGNAL(aboutToShow()),  SLOT(fill()) );
}


void LazyFillMenu::fill()
{
    if( Empty || Refill )
    {
        if( Refill )
            clear();

        Empty = false;

        Filler->fill( this );
    }
}

LazyFillMenu::~LazyFillMenu()
{
    delete Filler;
}


#include "lazyfillmenu.moc"
