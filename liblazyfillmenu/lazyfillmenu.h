/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef LAZYFILLMENU_H
#define LAZYFILLMENU_H

// qt specific
#include <qpopupmenu.h>


class MenuFiller;

/**
 * menu which does lazy filling
 * and does not even do this itself...
 * All it does is to kill his helper on exit
 * What a questionable behaviour... B)
 */
class LazyFillMenu : public QPopupMenu
{
    Q_OBJECT

public:
    LazyFillMenu( MenuFiller *CM, bool R, QWidget *Parent, const char *Name = 0 );
    virtual ~LazyFillMenu();

public:
    MenuFiller *filler() const;

public slots:
    void fill();

protected:
    /** */
    MenuFiller *Filler;
    /** true if menu should be refilled every time */
    const bool Refill;
    /** true if menu not filled */
    bool Empty;
};


inline MenuFiller *LazyFillMenu::filler() const { return Filler; }

#endif
