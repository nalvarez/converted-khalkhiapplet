/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qtooltip.h>
// KDE
#include <kiconloader.h>
#include <kglobal.h>
// libs
#include <lazyfillmenu.h>
#include <menubutton.h>
// Khalkhi
#include <propertystatusserviceclient.h>
#include <statuschange.h>
#include <services.h>
#include <icon.h>
#include <person.h>
// applet
#include "presenter.h"
#include "personmenufiller.h"
#include "personmenubuttonfiller.h"


class StatusClientButtonProxy : public Khalkhi::PropertyStatusServiceClient
{
public:
    StatusClientButtonProxy( PersonMenuButtonFiller *filler ) : m_filler( filler ) {}

public: // interface
    virtual const KABC::Addressee &person() const { return m_filler->person(); }

public: // slots interface
    virtual void onStateChange( const Khalkhi::PropertyStatusService &service, const Khalkhi::StatusChange &change,
                                const Khalkhi::Status &newStatus, int itemIndex );
    virtual void onPropertyManagerChange() { m_filler->updateImage(); }

protected:
    PersonMenuButtonFiller *m_filler;
};

void StatusClientButtonProxy::onStateChange( const Khalkhi::PropertyStatusService &service,
                                             const Khalkhi::StatusChange &change,
                                             const Khalkhi::Status &newStatus, int itemIndex )
{
    Q_UNUSED(newStatus)
    Q_UNUSED(itemIndex)

    m_filler->updateImage();
    MenuButton *button = m_filler->button();
    if( button )
    {
        if( !change.data( Khalkhi::EnabledRole ).asBool() )
            return;

        const QString eventId = change.data( Khalkhi::IdRole ).asString();
        const QString text = change.data( Khalkhi::DisplayTextRole ).asString();
        const QImage icon = change.data( Khalkhi::DisplayIconRole ).asImage();

        Khalkhi::Presenter::present( m_filler->person(), button, service.id(), eventId, icon, text );
    }
}


PersonMenuButtonFiller::PersonMenuButtonFiller( const KABC::Addressee &person )
: m_person( person ), m_button( 0 ), m_statusProxy( new StatusClientButtonProxy(this) )
{
    Khalkhi::Services::self()->registerClient( m_statusProxy );
}

void PersonMenuButtonFiller::fill( MenuButton *button )
{
    m_button = button;

    m_button->setMenu( new LazyFillMenu(new PersonMenuFiller(m_person),false,m_button) );
    m_button->setTitel( m_person.realName() );
    m_button->setDrawArrow( false );

    fillIcon( m_button );
}


void PersonMenuButtonFiller::fillIcon( MenuButton *button )
{
    QToolTip::add( button, Khalkhi::RichTexter::self()->createTip(m_person) );

    int pixmapSize = button->pixmapSize();
    // get some icon to display in front
    KABC::Picture picture = m_person.photo();
    if( picture.data().isNull() )
        picture = m_person.logo();

    // any picture?
    bool hasPicture = picture.isIntern() && !picture.data().isNull();

    QImage pixmap = hasPicture ? picture.data()
                    : KGlobal::iconLoader()->loadIcon( "personal",KIcon::NoGroup,pixmapSize ).convertToImage();

    Khalkhi::Icon icon( pixmapSize );
    icon.appendIcon( pixmap );
    icon.fill( m_person );

    button->setIcon( icon.data() );
}


void PersonMenuButtonFiller::updateImage()
{
    if( m_button ) fillIcon( m_button );
}


PersonMenuButtonFiller::~PersonMenuButtonFiller()
{
    Khalkhi::Services::self()->unregisterClient( m_statusProxy );

    delete m_statusProxy;
}
