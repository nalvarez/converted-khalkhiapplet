/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KHALKHIAPPLET_H
#define KHALKHIAPPLET_H


// qt specific
#include <qstring.h>
#include <qvaluelist.h>
// kde specific
#include <kabc/stdaddressbook.h>
#include <kpanelapplet.h>
// Khalkhi specific
#include <services.h>
// libs specific
#include <menubutton.h>
// applet
#include <personlistmenufiller.h>


using namespace KABC;

class QPopupMenu;
class KhalkhiAppletSettings;
namespace Khalkhi {
class AllPropertiesGlobalDataActionServiceMenuFiller;
class ListAllPropertiesGlobalDataActionServiceMenuFiller;
}
enum ButtonType { AllButton=0, CategoryButton=1, PersonButton=2 };


class KhalkhiApplet : public KPanelApplet
{
    Q_OBJECT
public:
  static const char * const AppletName;

public:
    KhalkhiApplet( const QString& ConfigFile, QWidget *Parent, const char *Name );
    virtual ~KhalkhiApplet();

protected: // KPanelApplet API
    virtual int widthForHeight( int Height) const;
    virtual int heightForWidth( int Width ) const;
    virtual void about();
    virtual void preferences();
    virtual void positionChange( Position P );

protected: // QWidget API
    virtual void resizeEvent( QResizeEvent * );
    virtual void mousePressEvent( QMouseEvent *ME );

    virtual void dragMoveEvent( QDragMoveEvent *Event );
    virtual void dropEvent( QDropEvent *Event );

protected:
    void arrangeButtons();
    void fillButtons();
    void addButton( ButtonType Type );

    void addPersonButton( const QString &UID );
    void addListButton( const QString &CategoryName, const QString &IconName, KNameType NameType,
                            const QString &FieldName, Qt::SortOrder SortOrder, bool GroupServices );

    void appendButton( MenuButton *Button );
    void removeButton( MenuButton *Button );
    void configureButton( MenuButton *Button );

    void writeConfig();

protected:
    MenuButton* buttonAt( const QPoint Pos ) const;

protected slots:
    void launchAddressbook();
    void onAddressBookChange();
    void onPersonsChange();

protected:
    QValueList<MenuButton*> ButtonList;

    int PixmapSize;

    KABC::AddressBook *Book;
    Khalkhi::AllPropertiesGlobalDataActionServiceMenuFiller *DropServices;
    Khalkhi::ListAllPropertiesGlobalDataActionServiceMenuFiller *ListDropServices;

//     QPopupMenu *ContextMenu;
    KhalkhiAppletSettings *Settings;
};

#endif
