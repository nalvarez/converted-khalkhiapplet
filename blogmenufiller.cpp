/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// qt specific
#include <qpopmenu.h>
// kde specific
#include <kmimetype.h>
#include <kiconloader.h>
#include <kapp.h>
#include <klocale.h>
// menu specific
#include "lazyfillmenu.h"
#include "blogmenufiller.h"


static const int BlogBaseId = 1000;

void BlogMenuFiller::createMenuEntry( const KABC::Addressee Contact, QPopupMenu *Menu )
{
    const QString BlogFeed = Contact.custom( "KADDRESSBOOK" , "BlogFeed" );
    if( !BlogFeed.isEmpty() )
    {
        LazyFillMenu *SubMenu = new LazyFillMenu( new BlogMenuFiller(BlogFeed), true, Menu );
        Menu->insertItem( SmallIconSet("news"), i18n("&Blog"), SubMenu );
    }
}


BlogMenuFiller::BlogMenuFiller( const KURL U )
: FeedURL( U ), CurrentMenu( 0 )
{
}


int BlogMenuFiller::fill( QPopupMenu *Menu )
{
    CurrentMenu = Menu;

    // TODO: knowledge about refilling is spread over too many places!
    Menu->disconnect( this );
    connect( Menu, SIGNAL(activated(int)), this, SLOT(browse(int)) );

    // fill temporary TODO: put rotating icon and nongreyed, unclickable entry there
    int id = Menu->insertItem( i18n("Loading feed...") );
    Menu->setItemEnabled( id, false );

    // start the rss fetching
    RSS::Loader *RSSLoader = RSS::Loader::create();
    connect( RSSLoader, SIGNAL( loadingComplete( Loader*, Document, Status ) ),
             this, SLOT( loadingComplete( Loader*, Document, Status ) ) );
    RSSLoader->loadFrom( FeedURL, new RSS::FileRetriever );

/* oder gleich warten? Könnte zu lange dauern
    IsLoading = true;
    while( IsLoading ) {
        qApp->eventLoop()->processEvents( QEventLoop::ExcludeUserInput );
        usleep( 500 );
    }
*/
    return 1;
}



void BlogMenuFiller::browse( int id ) const
{
    if( id >= BlogBaseId )
    {
        kapp->invokeBrowser( (*ArticleList.at(id-BlogBaseId)).link().url() );
    }
}


void BlogMenuFiller::loadingComplete( RSS::Loader*,
                                      RSS::Document RSSDocument,
                                      RSS::Status RSSStatus )
{
    // remove temp entry
    CurrentMenu->clear();

    ArticleList = RSSDocument.articles();

    // check if 
    QString ErrorReport;

    if( RSSStatus != RSS::Success )
    {
        if( RSSStatus == RSS::RetrieveError )
            ErrorReport = i18n( "Feed not accessable." );
        else if( RSSStatus == RSS::ParseError )
            ErrorReport = i18n( "Feed not parseable." );
        else
            ErrorReport = i18n( "Unknown problem." );
    }
    else
        if( ArticleList.isEmpty() )
            ErrorReport = i18n( "No articles." );

    if( !ErrorReport.isEmpty() )
    {
        int id = CurrentMenu->insertItem( ErrorReport );
        CurrentMenu->setItemEnabled( id, false );
        return;
    }

    // fill titles in menu
    RSS::Article::List::ConstIterator it = ArticleList.begin();
    int i = BlogBaseId;
    for( ; it != ArticleList.end(); ++it,++i )
        CurrentMenu->insertItem( KMimeType::pixmapForURL((*it).link(),0,KIcon::Small), 
                                 escapeAmpersand((*it).title()), i );
}


#include "blogmenufiller.moc"
