/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PERSONLISTMENUFILLER_H
#define PERSONLISTMENUFILLER_H


// menu specific
#include "addresseelistspan.h"
#include "menufiller.h"

enum KNameType { DisplayFormattedName=0, DisplayNickname=1, DisplayGivenName=2, DisplayFamilyName=3 };

class LazyFillMenu;


class PersonListMenuFiller : public MenuFiller
{
public:
    PersonListMenuFiller( const AddresseeListSpan Span, KNameType NameType,
                           const QString &FirstToken, const QString &LastToken );
    PersonListMenuFiller( const AddresseeListSpan Span, KNameType NameType );

public: // MenuFiller API
    virtual int fill( QPopupMenu *Menu );

protected:
    static void createTokens( QString &PreviousToken, QString &NextToken,
                              const QString &Previous, const QString &Next );

protected:
   QString nameForPerson( const KABC::Addressee &Person );

protected:
    /** sublist of persons to care of */
    const AddresseeListSpan PersonsSpan;
    /** */
    QString UpperFirstEntryToken;
    QString UpperLastEntryToken;

    KNameType NameType;
};


inline QString PersonListMenuFiller::nameForPerson( const KABC::Addressee &Person )
{
    QString Name;
    switch( NameType )
    {
    case DisplayNickname: Name = Person.nickName(); break;
    case DisplayGivenName: Name = Person.givenName(); break;
    case DisplayFamilyName: Name = Person.familyName(); break;
    case DisplayFormattedName: ;
    default:       Name = Person.realName();
    }
    return Name; 
}

#endif
