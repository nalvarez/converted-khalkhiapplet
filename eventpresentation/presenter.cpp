/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qmime.h>
// KDE
#include <kconfig.h>
#include <kpassivepopup.h>
#include <kabc/addressee.h>
#include <kaudioplayer.h>
// Khalkhi
#include <person.h>
// applet
#include "menubutton.h"
#include "presentation.h"
#include "presenter.h"


static const char presentationId[] =         "presentation";
static const char presentationMaskId[] =     "presentationMask";
static const char defaultPresentationId[] =  "default_presentation";
static const char soundFileUrlId[] =         "soundfile";
static const char defaultSoundFileUrlId[] =  "default_sound";

namespace Khalkhi {

void Presenter::present( const KABC::Addressee &person,
                         MenuButton *button,
                         const QString &serviceId, const QString &eventId,
                         const QImage &icon, const QString &text )
{
    KConfig eventConfig( QString::fromLatin1("khalkhi/services/%1/eventsrc").arg(serviceId), true, false, "data" );
    eventConfig.setGroup( eventId );

    KConfig presentationConfig( QString::fromLatin1("khalkhi/%1.eventsrc").arg(serviceId), true, false );
    presentationConfig.setGroup( eventId );

    KConfig personPresentationConfig( QString::fromLatin1("khalkhi/persons/%1/%2.eventsrc").arg(person.uid(),serviceId),
                                      true, false );
    personPresentationConfig.setGroup( eventId );


    // get event presentation
    static const int Undefined = -1;
    static const int NoMask = 0;

    int personPresentation = personPresentationConfig.readNumEntry( presentationId, Undefined );
    int personPresentationMask = personPresentationConfig.readNumEntry( presentationMaskId, NoMask );

    int presentation = presentationConfig.readNumEntry( presentationId, Undefined );
    if( presentation == Undefined )
        presentation = eventConfig.readNumEntry( defaultPresentationId, NoPresentation );

    presentation = (presentation & ~personPresentationMask) | (personPresentation & personPresentationMask);


    // get sound file name
    QString soundFileURL;

    if( presentation & Khalkhi::Sound )
    {
        soundFileURL = personPresentationConfig.readPathEntry( soundFileUrlId );
        if( soundFileURL.isEmpty() )
            soundFileURL = presentationConfig.readPathEntry( soundFileUrlId );
        if( soundFileURL.isEmpty() )
            soundFileURL = eventConfig.readPathEntry( defaultSoundFileUrlId );
    }

    // 
    if( presentation & Khalkhi::Sound )
        KAudioPlayer::play( soundFileURL );

    if( presentation & PanelButton )
        button->blink();

    if( presentation & PassivePopup )
    {
        QString tip;
        if( !icon.isNull() )
        {
            const QString iconId = QString::fromLatin1( "statuschangepopup" );//"%1_tipicon_%2_%3_%4" )
//               .arg( Status->id(), Filler->person().uid(), PropertyId, QString::number(ItemIndex) );
            QMimeSourceFactory::defaultFactory()->setImage( iconId, icon );
            tip = QString::fromLatin1( "<img src=\"%1\">&nbsp;%2" ).arg( iconId, text );
        }
        else
            tip = text;

        tip = Khalkhi::RichTexter::self()->createTip( person, tip );

        KPassivePopup::message( tip, button );
    }
}

}
