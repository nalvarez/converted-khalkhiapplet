/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PERSONLISTSERVICEMENUFILLER_H
#define PERSONLISTSERVICEMENUFILLER_H

// KDE
#include <kabc/addresseelist.h>
// Khalkhi
#include <services.h>
#include <listallpropertiesglobalactionservicemenufiller.h>
#include <listactionservicemenufiller.h>
// libs
#include <menufiller.h>


class PersonListServiceMenuFiller : public MenuFiller
{
public:
    static void createMenuEntry( const KABC::AddresseeList &PersonList,
                                 const QString &IconName, const QString &Name, QPopupMenu *Menu );

public:
    PersonListServiceMenuFiller( const KABC::AddresseeList &PersonList );

public: // MenuFiller API
    virtual int fill( QPopupMenu *Menu );

protected:
    QPopupMenu *Menu;
    const KABC::AddresseeList PersonList;

    Khalkhi::ListAllPropertiesGlobalActionServiceMenuFiller ListAllPropertiesServicesMenuFiller;
    Khalkhi::ListActionServiceMenuFiller PersonListServicesMenuFiller;
};

#endif
