/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PERSONMENUBUTTONFILLER_H
#define PERSONMENUBUTTONFILLER_H


// KDE
#include <kabc/addressee.h>
// libs
#include <menubuttonfiller.h>

class StatusClientButtonProxy;


class PersonMenuButtonFiller : public MenuButtonFiller
{
public:
    PersonMenuButtonFiller( const KABC::Addressee &person );
    virtual ~PersonMenuButtonFiller();

public: // MenuButtonFiller API
    virtual void fill( MenuButton *button );
    virtual void fillIcon( MenuButton *button );

public:
    const KABC::Addressee &person() const;
    MenuButton *button() const;

public:
    void updateImage();

protected:
    const KABC::Addressee m_person;
    MenuButton *m_button;

    StatusClientButtonProxy *m_statusProxy;
};

inline const KABC::Addressee &PersonMenuButtonFiller::person() const { return m_person; }
inline MenuButton *PersonMenuButtonFiller::button()            const { return m_button; }

#endif
