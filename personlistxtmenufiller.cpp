/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/



// qt specific
#include <qpopupmenu.h>
// kde specific
// applet specific
#include "personlistservicemenufiller.h"
#include "personlistxtmenufiller.h"


PersonListExtendedMenuFiller::PersonListExtendedMenuFiller( const KABC::AddresseeList &CL,
                                                            const QString &IN, const QString &T,
                                                            KNameType NameType )
 : PersonListMenuFiller( CL, NameType ), PersonList( CL ), IconName( IN ), Titel( T )
{
}


int PersonListExtendedMenuFiller::fill( QPopupMenu *Menu )
{
    PersonListServiceMenuFiller::createMenuEntry( PersonList, IconName, Titel, Menu );
    Menu->insertSeparator();

    return PersonListMenuFiller::fill( Menu );
}
