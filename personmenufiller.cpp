/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/



// qt specific
// kde specific
#include <kiconloader.h>
// libs specific
#include <lazyfillmenu.h>
// applet specific
#include "personmenufiller.h"


static const char AppletContextId[] = "KhalkhiApplet";

void PersonMenuFiller::createMenuEntry( const KABC::Addressee &Person, const QString &Name, QPopupMenu *Menu )
{
    // get some icon to display in front
    KABC::Picture ABCPicture = Person.photo();
    if( ABCPicture.data().isNull() )
        ABCPicture = Person.logo();

    QPixmap Pixmap;
    if( ABCPicture.isIntern() && !ABCPicture.data().isNull() )
    {
        int w = KGlobal::iconLoader()->currentSize( KIcon::Small );
        Pixmap = ABCPicture.data().smoothScale( w, w, QImage::ScaleMin );
    }
    else 
        Pixmap = SmallIcon( "personal" );

    // menu
    LazyFillMenu *SubMenu = new LazyFillMenu( new PersonMenuFiller(Person), false, Menu );
    // put it in
    Menu->insertItem( Pixmap, escapeAmpersand(Name), SubMenu );
}


PersonMenuFiller::PersonMenuFiller( const KABC::Addressee &P )
: Person( P )
{
    PersonServicesMenuFiller.setContext( AppletContextId );
}


int PersonMenuFiller::fill( QPopupMenu *Menu )
{
    int MenuBaseId = Menu->count();

    AllPropertiesServicesMenuFiller.set( Person );
    AllPropertiesServicesMenuFiller.fillMenu( Menu, MenuBaseId );

    PersonServicesMenuFiller.set( Person );
    PersonServicesMenuFiller.fillMenu( Menu, Menu->count() );

    return Menu->count() - MenuBaseId;
}
