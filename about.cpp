/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/


// program specific
#include "khalkhiapplet.h"
#include "about.h"


// Applet
static const char AppletTitel[] =        I18N_NOOP("People Applet");
static const char AppletDescription[] = I18N_NOOP("Enables quick access to the entries in your addressbook");
static const char AppletVersion[] =     "0.5.3";
static const char AppletCopyright[] =   "(C) 2006-2007 Friedrich W. H. Kossebau";
// Author
static const char FWHKName[] =         "Friedrich W. H. Kossebau";
static const char FWHKTask[] =         I18N_NOOP("Author");
static const char FWHKEmailAddress[] = "kossebau@kde.org";


KhalkhiAppletAboutData::KhalkhiAppletAboutData()
: KAboutData( KhalkhiApplet::AppletName, AppletTitel, AppletVersion, AppletDescription,
              KAboutData::License_GPL_V2, AppletCopyright, 0, 0, FWHKEmailAddress )
{
    addAuthor( FWHKName, FWHKTask, FWHKEmailAddress );
}
