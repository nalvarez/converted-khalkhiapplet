/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qtooltip.h>
// kde specific
#include <kglobal.h>
#include <kiconloader.h>
// lib specific
#include "lazyfillmenu.h"
#include "menubutton.h"
#include "personlistmenufiller.h"
#include "personlistxtmenufiller.h"
#include "personlistmenubuttonfiller.h"

PersonListMenuButtonFiller::PersonListMenuButtonFiller( const KABC::AddresseeList &List,
                                                        const QString &IN,
                                                        KNameType NT,
                                                        const QString &T, bool IC )
 : PersonList( List ),
   IconName( IN ), NameType( NT ), SortField( 0 ), SortOrder( Qt::Ascending ), GroupServices( false ),
   Titel( T ), IsCategory( IC )
{
}


void PersonListMenuButtonFiller::setNameType( KNameType NewType )
{
    NameType = NewType;
}

void PersonListMenuButtonFiller::setIconName( const QString &NewName )
{
    IconName = NewName;
}

void PersonListMenuButtonFiller::setSortField( KABC::Field *NewSortField )
{
    SortField = NewSortField;
}

void PersonListMenuButtonFiller::setSortOrder( Qt::SortOrder NewSortOrder )
{
    SortOrder = NewSortOrder;
}

void PersonListMenuButtonFiller::setGroupServices( bool GS )
{
    GroupServices = GS;
}


void PersonListMenuButtonFiller::fill( MenuButton *Button )
{
    PersonList.setReverseSorting( SortOrder == Qt::Descending );
    if( SortField )
        PersonList.sortByField( SortField );

    //int Style = ShowFaceBefore | FirstFullName | TwoRows;

    //QToolTip::add( Button, PersonRichTexter::self()->createPersonDesc(Person,Style) );
    QToolTip::add( Button, Titel );

    MenuFiller *Filler = GroupServices ?
         new PersonListExtendedMenuFiller( PersonList, IconName, Titel, NameType ) :
         new PersonListMenuFiller( PersonList, NameType );
    Button->setMenu( new LazyFillMenu(Filler,true,Button) );
    Button->setTitel( Titel );

    fillIcon( Button );
}


void PersonListMenuButtonFiller::fillIcon( MenuButton *Button )
{
    // TODO: add image to personlist, then put it onto addressbook icon as emblem

    QPixmap Icon = KGlobal::iconLoader()->loadIcon( IconName,
                                                    KIcon::Panel, Button->pixmapSize(),
                                                    KIcon::DefaultState, 0, true );

    Button->setIcon( Icon );
}

KABC::AddresseeList PersonListMenuButtonFiller::personList() const { return PersonList; }

PersonListMenuButtonFiller::~PersonListMenuButtonFiller() {}
