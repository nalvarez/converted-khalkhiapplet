/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef CATEGORYSELECTDIALOG_H
#define CATEGORYSELECTDIALOG_H


// kde specific
#include <kdialogbase.h>

class QComboBox;
namespace KABC {
class Addressee;
class AddressBook;
}


class KCategorySelectDialog : public KDialogBase
{
public:
    static QString getCategory( const QString& Caption, const QString& Label,
                                KABC::AddressBook *Book, bool *Ok, QWidget *Parent = 0 );

public:
    KCategorySelectDialog( const QString& Caption, const QString& Label, KABC::AddressBook *Book, QWidget *Parent = 0 );
    virtual ~KCategorySelectDialog();

public:
//     virtual QString makeCompletion( const QString &string );

public:
//     void setContacts( const KABC::Addressee::List &list );

protected:
    QComboBox *ComboBox;
};

#endif
