/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// qt
#include <qmap.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qcombobox.h>
// kde
#include <kiconloader.h>
#include <kabc/addressee.h>
#include <kabc/addressbook.h>
//
#include "kcategoryselectdialog.h"



static QStringList getCategories( const KABC::AddressBook *Book )
{
    // the result list
    typedef QMap<QString,int> NameList; // TODO: remove hack to get sorted list with no duplicates
    NameList AllCategoriesMap;

    KABC::AddressBook::ConstIterator ait = Book->begin();
    KABC::AddressBook::ConstIterator endAIt = Book->end();

    // get the categories of all contacts
    for ( ; ait != endAIt; ++ait )
    {
        const QStringList Categories( (*ait).categories() );
        QStringList::ConstIterator end = Categories.end();

        for ( QStringList::ConstIterator it = Categories.begin(); it != end; ++it )
            AllCategoriesMap.insert( *it, 0 );
    }

    QStringList AllCategories( AllCategoriesMap.keys() );
    AllCategories.sort();

    return AllCategories;
}

QString KCategorySelectDialog::getCategory( const QString& Caption, const QString& Label,
                                            KABC::AddressBook *Book, bool *Ok, QWidget *Parent )
{
    QString Result;

    KCategorySelectDialog Dialog( Caption, Label, Book, Parent );

    bool Selected = ( Dialog.exec() == Accepted );

    if( Ok )
        *Ok = Selected;

    if( Selected )
        Result = Dialog.ComboBox->currentText();

    return Result;
}

KCategorySelectDialog::KCategorySelectDialog( const QString& Caption, const QString& Text,
                                            KABC::AddressBook *Book, QWidget *Parent )
 : KDialogBase( Parent, 0, true, Caption, Ok|Cancel, Ok, true )
{
    // base 
    QWidget *Page = new QWidget( this );
    setMainWidget( Page );
    QVBoxLayout *PageLayout = new QVBoxLayout( Page, 0, spacingHint() );

    // 
    QLabel *Label = new QLabel( Text, Page );

    ComboBox = new QComboBox( Page );

    PageLayout->addWidget( Label );
    PageLayout->addWidget( ComboBox );

    PageLayout->addStretch( 10 );

    ComboBox->insertStringList( getCategories(Book) );
}

KCategorySelectDialog::~KCategorySelectDialog()
{}
