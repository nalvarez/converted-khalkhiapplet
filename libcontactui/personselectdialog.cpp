/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// qt
#include <qwidget.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qcombobox.h>
// kde
#include <kiconloader.h>
#include <kabc/addressee.h>
#include <kabc/addressbook.h>
//
#include "personselectdialog.h"


KABC::Addressee PersonSelectDialog::getPerson( const QString& Caption, const QString& Label,
                                               KABC::AddressBook *Book, bool *Ok, QWidget *Parent )
{
    KABC::Addressee Result;

    PersonSelectDialog Dialog( Caption, Label, Book, Parent );

    bool Selected = ( Dialog.exec() == Accepted );

    if( Ok )
        *Ok = Selected;

    if( Selected )
        Result = Dialog.currentPerson();

    return Result;
}

PersonSelectDialog::PersonSelectDialog( const QString& Caption, const QString& Text,
                                        KABC::AddressBook *Book, QWidget *Parent )
 : KDialogBase( Parent, 0, true, Caption, Ok|Cancel, Ok, true )
{
    // base 
    QWidget *Page = new QWidget( this );
    setMainWidget( Page );
    QVBoxLayout *PageLayout = new QVBoxLayout( Page, 0, spacingHint() );

    // 
    QLabel *Label = new QLabel( Text, Page );

    ComboBox = new QComboBox( Page );

    PageLayout->addWidget( Label );
    PageLayout->addWidget( ComboBox );

    PageLayout->addStretch( 10 );

    AddressList = Book->allAddressees();
    AddressList.sortBy( KABC::FormattedName );
    KABC::AddresseeList::ConstIterator it;
    for( it = AddressList.begin(); it != AddressList.end(); ++it )
    {
        // get some icon to display in front
        KABC::Picture ABCPicture = (*it).photo();
        if( ABCPicture.data().isNull() )
            ABCPicture = (*it).logo();

        QPixmap Pixmap;
        if( ABCPicture.isIntern() && !ABCPicture.data().isNull() )
        {
            int w = KGlobal::iconLoader()->currentSize( KIcon::Small );
            Pixmap = ABCPicture.data().smoothScale( w, w, QImage::ScaleMin );
        }
        else 
            Pixmap = SmallIcon( "personal" );
        ComboBox->insertItem( Pixmap, (*it).formattedName() );
    }
}

KABC::Addressee PersonSelectDialog::currentPerson() const
{
    const int CurrentIndex = ComboBox->currentItem();

    return (CurrentIndex!=-1) ? AddressList[CurrentIndex] : KABC::Addressee();
}


PersonSelectDialog::~PersonSelectDialog() {}
