/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/



// qt specific
// kde specific
#include <klocale.h>
#include <kiconloader.h>
// libs specific
#include <lazyfillmenu.h>
// applet specific
#include "personlistservicemenufiller.h"


static const char AppletContextId[] = "KhalkhiApplet";

//TODO: add pixmap of button
void PersonListServiceMenuFiller::createMenuEntry( const KABC::AddresseeList &PersonList,
                                                    const QString &IconName, const QString &Name, QPopupMenu *Menu )
{
    // menu
    LazyFillMenu *SubMenu = new LazyFillMenu( new PersonListServiceMenuFiller(PersonList), false, Menu );
    // put it in
    Menu->insertItem( SmallIcon(IconName), escapeAmpersand(Name), SubMenu );
}


PersonListServiceMenuFiller::PersonListServiceMenuFiller( const KABC::AddresseeList &CL )
: PersonList( CL )
{
    PersonListServicesMenuFiller.setContext( AppletContextId );
}


int PersonListServiceMenuFiller::fill( QPopupMenu *Menu )
{
    int MenuBaseId = Menu->count();

    ListAllPropertiesServicesMenuFiller.setPersonList( PersonList );
    ListAllPropertiesServicesMenuFiller.fillMenu( Menu, MenuBaseId );

    PersonListServicesMenuFiller.setPersonList( PersonList );
    PersonListServicesMenuFiller.fillMenu( Menu, Menu->count() );

    const int Inserted = Menu->count() - MenuBaseId;
    if( Inserted == 0 )
    {
        int id = Menu->insertItem( i18n("No Entries"), 0 );
        Menu->setItemEnabled( id, false );
    }

    return Inserted;
}
