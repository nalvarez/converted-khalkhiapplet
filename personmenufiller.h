/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PERSONMENUFILLER_H
#define PERSONMENUFILLER_H

// KDE 
#include <kabc/addressee.h>
// Khalkhi 
#include <services.h>
#include <allpropertiesglobalactionservicemenufiller.h>
#include <actionservicemenufiller.h>
//
#include <menufiller.h>


class PersonMenuFiller : public MenuFiller
{
public:
    static void createMenuEntry( const KABC::Addressee &Person, const QString &Name, QPopupMenu *Menu );

public:
    PersonMenuFiller( const KABC::Addressee &Person );

public: // MenuFiller API
    virtual int fill( QPopupMenu *Menu );

protected:
    const KABC::Addressee Person;

    Khalkhi::AllPropertiesGlobalActionServiceMenuFiller AllPropertiesServicesMenuFiller;
    Khalkhi::ActionServiceMenuFiller PersonServicesMenuFiller;
};

#endif
