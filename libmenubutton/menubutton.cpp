/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qimage.h>
#include <qtimer.h>
#include <qtooltip.h>
#include <qpainter.h>
#include <qstyle.h>
// KDE
#include <kdebug.h>
#include <kglobal.h>
#include <kiconeffect.h>
#include <kicontheme.h>
#include <kiconloader.h>
#include <kapplication.h>
// library
#include "lazyfillmenu.h"
#include "menubuttonfiller.h"
#include "menubutton.h"


static const int BlinkTimeOut = 8;
static const int BlinkTimeMS = 500;
static const int NotBlinking = -1;

static const int DownOffset = 2;
static const int Spacing = 2;
static const int ArrowSize = 8;

MenuButton::MenuButton( MenuButtonFiller* F, QWidget* parent, const char* name )
: QButton( parent, name ), Filler( F ), Menu( 0 ), PixmapSize( 0 ),
  DrawArrow( true ), Highlighted( false ),
  BlinkState( NotBlinking ),
  BlinkTimer( new QTimer(this) )
{
    // do
    Filler->fill( this );

    connect( this, SIGNAL(pressed()), SLOT(showMenu()) );
    connect( BlinkTimer, SIGNAL(timeout()), SLOT(onBlinkTimer()) );
}


void MenuButton::setPanelPosition( KPanelApplet::Position P )
{
    switch( P )
    {
        case KPanelApplet::pBottom:
            PanelOrientation = Horizontal;
            PopupDirection = KPanelApplet::Up;
            ArrowDirection = KPanelExtension::Top;
            break;
        case KPanelApplet::pTop:
            PanelOrientation = Horizontal;
            PopupDirection = KPanelApplet::Down;
            ArrowDirection = KPanelExtension::Bottom;
            break;
        case KPanelApplet::pRight:
            PanelOrientation = Vertical;
            PopupDirection = KPanelApplet::Left;
            ArrowDirection = KPanelExtension::Left;
            break;
        case KPanelApplet::pLeft:
            PanelOrientation = Vertical;
            PopupDirection = KPanelApplet::Right;
            ArrowDirection = KPanelExtension::Right;
            break;
    }
}


void MenuButton::setIcon( const QPixmap &Icon )
{
    // create normal copy
    NormalPixmap = Icon;

    // create highlighted copy
    KIconEffect *Effect = KGlobal::iconLoader()->iconEffect();
    if( Effect )
    {
        QImage HighlightImage = Icon.convertToImage();
        HighlightImage = Effect->apply( HighlightImage, KIcon::Panel, KIcon::ActiveState );

        HighlightPixmap.convertFromImage(HighlightImage);
    }
    else
        HighlightPixmap = NormalPixmap;

    setPixmap( Highlighted ? HighlightPixmap : NormalPixmap );
}


void MenuButton::setMenu( LazyFillMenu *M )
{
    delete Menu;

    Menu = M;

    if( Menu )
    {
        Menu->installEventFilter( this );
        connect( Menu, SIGNAL(aboutToHide()), this, SLOT(menuAboutToHide()) );
    }

}


void MenuButton::setTitel( const QString &T )
{
    Titel = T;
}


void MenuButton::setDrawArrow( bool DA )
{
    if( DrawArrow == DA )
        return;

    DrawArrow = DA;
    update();
}


void MenuButton::drawButton( QPainter *Painter )
{
    // background
    if( paletteBackgroundPixmap() )
        Painter->drawPixmap(0, 0, *paletteBackgroundPixmap());

    // frame
    if( isDown() )
        // Draw shapes to indicate the down state.
        style().drawPrimitive( QStyle::PE_Panel, Painter, rect(), colorGroup(), QStyle::Style_Sunken );

    drawButtonLabel( Painter );

    if( hasFocus() )
    {
        int x1, y1, x2, y2;
        rect().coords( &x1, &y1, &x2, &y2 );
        QRect FocusRect( x1+Spacing, y1+Spacing, x2-x1-Spacing-1, y2-y1-Spacing-1 );
        style().drawPrimitive( QStyle::PE_FocusRect, Painter, FocusRect, colorGroup(),
                               QStyle::Style_Default, colorGroup().button() );
    }
}


void MenuButton::drawButtonLabel( QPainter *Painter )
{
    bool IsDown = isDown();

    // draw pixmap
    const QPixmap *Pixmap = pixmap();
    if( Pixmap )
    {
        QPixmap P = *Pixmap;
        if (IsDown)
            P = P.convertToImage().smoothScale(P.width() - 2, P.height() - 2);

        int x = (width()  - P.width() ) / 2;
        int y = (height() - P.height()) / 2;
        Painter->drawPixmap( x, y, P );
    }

    // draw Arrow
    if( DrawArrow )
    {
        QStyle::PrimitiveElement Arrow;
        QRect ArrowRect( 0, 0, ArrowSize, ArrowSize );

        switch (ArrowDirection)
        {
            case KPanelExtension::Top:
                Arrow = QStyle::PE_ArrowUp;
                break;
            case KPanelExtension::Bottom:
                Arrow = QStyle::PE_ArrowDown;
                ArrowRect.moveBy( 0, height() - ArrowSize );
                break;
            case KPanelExtension::Right:
                Arrow = QStyle::PE_ArrowRight;
                ArrowRect.moveBy( width() - ArrowSize , 0 );
                break;
            case KPanelExtension::Left:
                Arrow = QStyle::PE_ArrowLeft;
                break;
            case KPanelExtension::Floating:
            default:
                Arrow = (orientation() == Horizontal) ?   QStyle::PE_ArrowDown :
                        (QApplication::reverseLayout()) ? QStyle::PE_ArrowLeft :
                        QStyle::PE_ArrowRight;
        }

        int Flags = QStyle::Style_Enabled;
        if( IsDown )
            Flags |= QStyle::Style_Down;

        style().drawPrimitive( Arrow, Painter, ArrowRect, colorGroup(), Flags );
    }

//    if( IsDown )
//        Painter->translate( -DownOffset, -DownOffset );
}


void MenuButton::showMenu()
{
    PressedDuringPopup = false;
    // kill 
    kapp->syncX();
    kapp->processEvents();

    Menu->fill();
    Menu->exec( menuPosition() );
}


bool MenuButton::calculatePixmapSize()
{
    bool Change = false;

    int NewSize = (orientation()==Horizontal ? height() : width()) - Spacing*2;

    if( NewSize >= 0 && PixmapSize != NewSize )
    {
        // Size has changed, update
        PixmapSize = NewSize;
        Change = true;
    }

    return Change;
}


QPoint MenuButton::menuPosition() const
{
    QPoint Pos;

    QRect GlobalRect( mapToGlobal(QPoint( 0, 0 )),
                      mapToGlobal(QPoint( width(), height() )) );

    QSize Size = Menu->sizeHint();

    switch (PopupDirection)
    {
        case KPanelApplet::Left:
        case KPanelApplet::Right:
        {
kdDebug() << "left/right" << endl;
            GlobalRect.setLeft( topLevelWidget()->x() );
            GlobalRect.setWidth( topLevelWidget()->width() );

            QDesktopWidget* Desktop = QApplication::desktop();
            QRect ScreenRect = Desktop->screenGeometry(
              Desktop->screenNumber(const_cast<MenuButton*>(this)) );

            Pos = QPoint( (PopupDirection == KPanelApplet::Left) ?
                          GlobalRect.left() - Size.width() : GlobalRect.right() + 1,
                          GlobalRect.top() );

            // try placement on the screen
            if (Pos.y() + Size.height() > ScreenRect.bottom())
            {
                // line end to bottom of panel
                Pos.setY( GlobalRect.bottom() - Size.height() );

                if (Pos.y() < ScreenRect.top())
                {
                    // line end to bottom of screen
                    Pos.setY( ScreenRect.bottom() - Size.height() );

                    if (Pos.y() < ScreenRect.top())
                        Pos.setY( ScreenRect.top() );
                }
            }
            break;
        }
        case KPanelApplet::Up:
        case KPanelApplet::Down:
        default:
        {
kdDebug() << "up/down" << endl;
            GlobalRect.setTop( topLevelWidget()->y() );
            GlobalRect.setHeight( topLevelWidget()->height() );

            Pos = QPoint( 0,
                          (PopupDirection == KPanelApplet::Up) ?
                          GlobalRect.top() - Size.height() : GlobalRect.bottom() + 1 );

            if( QApplication::reverseLayout() )
            {
                Pos.setX( GlobalRect.right() - Size.width() + 1 );

                // try placement on the screen
                if (Pos.x() - Size.width() < 0)
                    Pos.setX( GlobalRect.left() );
            }
            else
            {
                QDesktopWidget* Desktop = QApplication::desktop();
                QRect ScreenRect = Desktop->screenGeometry(
                  Desktop->screenNumber(const_cast<MenuButton*>(this)) );
                Pos.setX( GlobalRect.left() );

                // try placement on the screen
                if (Pos.x() + Size.width() > ScreenRect.right())
                {
                    // line end to left side of panel
                    Pos.setX( GlobalRect.right() - Size.width() + 1 );

                    if (Pos.x() < ScreenRect.left())
                         // line end to left side of screen
                         Pos.setX( ScreenRect.left() );
                }
            }

        }
    }
    return Pos;
}


void MenuButton::blink()
{
    BlinkTimer->start( BlinkTimeMS );

    BlinkState = BlinkTimeOut;

    setPixmap( HighlightPixmap );
    repaint( false );
}

void MenuButton::onBlinkTimer()
{
    if( BlinkState > NotBlinking )
        --BlinkState;
    else
        BlinkTimer->stop();

    const bool OnBlink = ( BlinkState % 2 == 0 );

    setPixmap( OnBlink ? HighlightPixmap : NormalPixmap );
    repaint( false );
}

void MenuButton::menuAboutToHide()
{
    if( !Menu )
        return;

    setDown( false );
}


void MenuButton::resizeEvent( QResizeEvent* )
{
    if( calculatePixmapSize() )
        Filler->fillIcon( this );
}


void MenuButton::enterEvent( QEvent* Event )
{
    if( !Highlighted )
    {
        Highlighted = true;
        setPixmap( HighlightPixmap );
        repaint( false );
    }

    QButton::enterEvent( Event );
}

void MenuButton::leaveEvent( QEvent* Event )
{
    if( Highlighted )
    {
        Highlighted = false;
        setPixmap( NormalPixmap );
        repaint( false );
    }

    QButton::leaveEvent( Event );
}


bool MenuButton::eventFilter( QObject *, QEvent *Event )
{
    /*if( Event->type() == QEvent::MouseMove)
    {
        QMouseEvent *MouseEvent = static_cast<QMouseEvent *>( Event );
        if (rect().contains(mapFromGlobal(MouseEvent->globalPos())) &&
            ((MouseEvent->state() & ControlButton) != 0 ||
             (MouseEvent->state() & ShiftButton) != 0))
        {
            PanelButton::mouseMoveEvent(MouseEvent);
            return true;
        }
    }
    else */if( Event->type() == QEvent::MouseButtonPress ||
              Event->type() == QEvent::MouseButtonDblClick)
    {
        // pressed inside button?
        QMouseEvent *MouseEvent = static_cast<QMouseEvent *>( Event );
        if( rect().contains(mapFromGlobal(MouseEvent->globalPos())) )
        {
            PressedDuringPopup = true;
            return true;
        }
    }
    else if( Event->type() == QEvent::MouseButtonRelease )
    {
        QMouseEvent *MouseEvent = static_cast<QMouseEvent *>( Event );
        if( rect().contains(mapFromGlobal(MouseEvent->globalPos())) )
        {
            if( PressedDuringPopup && Menu )
                Menu->hide();

            return true;
        }
    }
    return false;
}

MenuButton::~MenuButton()
{
    delete Filler;
}

#include "menubutton.moc"
