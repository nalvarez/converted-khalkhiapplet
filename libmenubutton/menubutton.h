/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef MENUBUTTON_H
#define MENUBUTTON_H

// Qt
#include <qpixmap.h>
#include <qbutton.h>
class QTimer;
// KDE
#include <kpanelapplet.h>
#include <kpanelextension.h>
// library
class MenuButtonFiller;
class LazyFillMenu;


class MenuButton : public QButton
{
  Q_OBJECT

  public:
    MenuButton( MenuButtonFiller* F, QWidget* Parent, const char* Name = 0 );
    virtual ~MenuButton();

  public: // QButton API
    void drawButton( QPainter *Painter );
    void drawButtonLabel( QPainter *P );

  public: // QWidget API
    virtual void resizeEvent( QResizeEvent* );
    virtual void enterEvent( QEvent* Event );
    virtual void leaveEvent( QEvent* Event );
    virtual bool eventFilter( QObject *, QEvent *Event );

  public:
    Orientation orientation() const;
    KPanelApplet::Direction popupDirection() const;
    KPanelExtension::Position arrowDirection() const;
    MenuButtonFiller *filler() const;
    LazyFillMenu *menu() const;
    QString titel() const;
    int pixmapSize() const;

  public:
    void setIcon( const QPixmap &Icon );
    void setDrawArrow( bool DA = true );
    void setMenu( LazyFillMenu *Menu );
    void setTitel( const QString &T );

  public slots:
    void setPanelPosition( KPanelApplet::Position P );
    void showMenu();
    void blink();

  protected:
    bool calculatePixmapSize();
    QPoint menuPosition() const;

  protected slots:
    void menuAboutToHide();
    void onBlinkTimer();

  protected:
    /** */
    MenuButtonFiller *Filler;
    /** */
    LazyFillMenu *Menu;
    /** */
    QString Titel;

    /** current size of faces */
    int PixmapSize;
    /** orientation of the panel */
    Orientation PanelOrientation;
    /** */
    KPanelApplet::Direction PopupDirection;
    /** */
    KPanelExtension::Position ArrowDirection;

    bool PressedDuringPopup;
    bool DrawArrow;
    bool Highlighted;
    QPixmap NormalPixmap;
    QPixmap HighlightPixmap;

    int BlinkState;
    QTimer *BlinkTimer;
};


inline Qt::Orientation MenuButton::orientation()              const { return PanelOrientation; }
inline KPanelApplet::Direction MenuButton::popupDirection()   const { return PopupDirection; }
inline KPanelExtension::Position MenuButton::arrowDirection() const { return ArrowDirection; }

inline LazyFillMenu *MenuButton::menu()       const { return Menu; }
inline MenuButtonFiller *MenuButton::filler() const { return Filler; }
inline QString MenuButton::titel()            const { return Titel; }
inline int MenuButton::pixmapSize()           const { return PixmapSize; }

#endif
