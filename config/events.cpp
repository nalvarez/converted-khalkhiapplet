/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qstringlist.h>
// KDE
#include <klocale.h>
#include <kconfig.h>
// module
#include "presentation.h"
#include "events.h"


static const char GlobalGroupId[] = "!Global!";
static const char NameId[] =     "Name";
static const char IconNameId[] = "IconName";
static const char CommentId[] =  "Comment";

static const char DefaultGroupId[] = "<default>";

static const char PresentationsId[] = "presentation";
static const char SoundFileURLId[] =  "soundfile";
static const char DefaultPresentationsId[] = "default_presentation";
static const char DefaultSoundFileURLId[] =  "default_sound";


namespace Khalkhi {

EventGroup::EventGroup( const QString &eventsFilePath )
 : m_events( 0 )
{
    m_eventConfig = new KConfig( eventsFilePath, true, false, "data" );
    m_eventConfig->setGroup( QString::fromLatin1(GlobalGroupId) );
    m_icon =        m_eventConfig->readEntry( QString::fromLatin1(IconNameId), QString::fromLatin1("misc") );
    m_description = m_eventConfig->readEntry( QString::fromLatin1(CommentId),  i18n("No description available") );

    // get name of group
    int s = 0;
    for( int i=0; i<2; ++i )
        s = eventsFilePath.find( '/', s ) + 1;

    QString presentationsFilePath = eventsFilePath.mid( s );

    int p = presentationsFilePath.find( '/' );
    m_name = presentationsFilePath.left( p );
    presentationsFilePath[p] = '.';

    presentationsFilePath = QString::fromLatin1("khalkhi/") + presentationsFilePath;
    m_presentationConfig = new KConfig( presentationsFilePath, false, false );
}


const EventList &EventGroup::eventList()
{
    if( !m_events )
        reload();

    return *m_events;
}


void EventGroup::reload( bool revertToDefaults )
{
    m_dirty = revertToDefaults;

    if( m_events )
        m_events->clear();
    else
    {
        m_events = new EventList;
        m_events->setAutoDelete( true );
    }

    QStringList eventConfigGroupNames = m_eventConfig->groupList();
    QStringList::ConstIterator it = eventConfigGroupNames.begin();
    QStringList::ConstIterator endIt = eventConfigGroupNames.end();

    while( it != endIt )
    {
        const QString &configGroupName = *it;

        if( (configGroupName) != QString::fromLatin1(GlobalGroupId)
            && (configGroupName) != QString::fromLatin1(DefaultGroupId) )
        {
            m_eventConfig->setGroup( configGroupName );

            Event *event = new Event( this );
            event->setConfigName(  configGroupName );
            event->setName(        m_eventConfig->readEntry(NameId) );
            event->setDescription( m_eventConfig->readEntry(CommentId) );
//             event->setDontShow(    m_eventConfig->readNumEntry("nopresentation",0) );
            if( event->name().isEmpty() && event->description().isEmpty() )
                delete event;
            else
            {
                if( event->description().isEmpty() )
                    event->setDescription( event->name() );

                int defaultPresentations = m_eventConfig->readNumEntry( DefaultPresentationsId, PassivePopup );
                const QString defaultSoundFileURL = m_eventConfig->readPathEntry( DefaultSoundFileURLId );

                m_presentationConfig->setGroup( configGroupName );

                if( revertToDefaults )
                {
                    event->setPresentations( defaultPresentations );
                    event->setSoundFileURL(  defaultSoundFileURL );
                }
                else
                {
                    event->setPresentations( m_presentationConfig->readNumEntry(PresentationsId,defaultPresentations) );
                    event->setSoundFileURL(  m_presentationConfig->readPathEntry(SoundFileURLId,defaultSoundFileURL) );
                }

                m_events->append( event );
            }
        }

        ++it;
    }
}

void EventGroup::sync()
{
    if( !m_events || !m_dirty )
        return;

    EventListIterator it( *m_events );
    Event *event;
    while( (event = it.current()) != 0 )
    {
        m_presentationConfig->setGroup( event->configName() );
        m_presentationConfig->writeEntry(     PresentationsId, event->presentations() );
        m_presentationConfig->writePathEntry( SoundFileURLId,  event->soundFileURL() );

        ++it;
    }
    m_presentationConfig->sync();

    m_dirty = false;
}

EventGroup::~EventGroup()
{
    delete m_presentationConfig;
    delete m_eventConfig;
    delete m_events;
}


int EventGroupList::compareItems( QPtrCollection::Item item1, QPtrCollection::Item item2 )
{
    return (static_cast<EventGroup*>( item1 )->description() >=
        static_cast<EventGroup*>( item2 )->description()) ? 1 : -1;
}

}
