/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qlayout.h>
#include <qlabel.h>
#include <qtooltip.h>
// KDE
#include <kiconloader.h>
#include <klocale.h>
#include <kdialog.h>
#include <kcombobox.h>
// module
#include "presentation.h"
#include "presentationcontrol.h"
#include "eventlistview.h"
#include "eventgrouplistpresentationcontrol.h"


namespace Khalkhi {

EventGroupListPresentationControl::EventGroupListPresentationControl( QWidget *parent, const char *name )
 : QWidget( parent, name )
{
    QVBoxLayout *topLayout = new QVBoxLayout( this, 0, KDialog::spacingHint() );

    QHBoxLayout *eventLayout = new QHBoxLayout();

    QLabel *label = new QLabel( i18n("Status Service:"), this );

    m_eventGroupCombo = new KComboBox( false, this );
    connect( m_eventGroupCombo, SIGNAL(activated( const QString& )), SLOT(onEventGroupActivated( const QString& )) );

    eventLayout->addWidget( label );
    eventLayout->addWidget( m_eventGroupCombo, 10 );

    m_eventListView = new EventListView( this );
    connect( m_eventListView, SIGNAL(selectionChanged( QListViewItem * )),
             SLOT(onEventChange( QListViewItem * )) );
    connect( m_eventListView, SIGNAL(presentationToggled( Event *, int )),
             SLOT(onPresentationToggle( Event *, int )) );

    m_presentationControl = new PresentationControl( this );
    connect( m_presentationControl, SIGNAL(presentationChanged(int,bool)),
             SLOT(onPresentationControlChange(int,bool)) );

    topLayout->addLayout( eventLayout );
    topLayout->addWidget( m_eventListView );
    topLayout->addWidget( m_presentationControl );
}

void EventGroupListPresentationControl::setEventGroups( const EventGroupList &eventGroupList )
{
    m_eventGroupList = eventGroupList;
    m_eventGroupList.setAutoDelete( true );

    EventGroupListIterator it( m_eventGroupList );
    for( ; it.current(); ++it )
        m_eventGroupCombo->insertItem( SmallIcon(it.current()->icon()), it.current()->description() );

    m_eventListView->setEnabled( !m_eventGroupList.isEmpty() );
    m_presentationControl->setEnabled( !m_eventGroupList.isEmpty() );

    onEventGroupActivated( m_eventGroupCombo->currentText() );
}

void EventGroupListPresentationControl::save()
{
    EventGroupListIterator it( m_eventGroupList );
    for( ; it.current(); ++it )
        it.current()->sync();

#if 0
    kapp->dcopClient()->send( "khalkhi", "", "updateEvents()", "");

#endif
    emit changed( false );
}


void EventGroupListPresentationControl::reload()
{
    EventGroupListIterator it( m_eventGroupList );
    for( ; it.current(); ++it )
        it.current()->reload();

    m_eventListView->setEventList( m_currentEventGroup->eventList() );

    emit changed( false );
}

void EventGroupListPresentationControl::setToDefaults()
{
    EventGroupListIterator it( m_eventGroupList );
    for( ; it.current(); ++it )
        it.current()->reload( true );

    m_eventListView->setEventList( m_currentEventGroup->eventList() );

    emit changed( true );
}


void EventGroupListPresentationControl::onEventGroupActivated( const QString &description )
{
    EventGroupListIterator it( m_eventGroupList );
    for( ; it.current(); ++it )
    {
        if( it.current()->description() == description )
        {
            m_currentEventGroup = it.current();
            m_eventListView->setEventList( m_currentEventGroup->eventList() );
            break;
        }
    }
}

void EventGroupListPresentationControl::onEventChange( QListViewItem *i )
{
    EventListViewItem *item = static_cast<EventListViewItem*>( i );
    Event *event = item->event();

    int presentations = event->presentations();

    m_presentationControl->blockSignals( true );
    m_presentationControl->setPlaySound( presentations & Sound );
    m_presentationControl->setSoundFileURL( event->soundFileURL() );
    m_presentationControl->setShowPassivePopup( presentations & PassivePopup );
    m_presentationControl->setMarkButton( presentations & PanelButton );
    m_presentationControl->blockSignals( false );
}

void EventGroupListPresentationControl::onPresentationToggle( Event *event, int presentation )
{
    m_currentEventGroup->setDirty();

    m_presentationControl->blockSignals( true );

    const int presentations = event->presentations();

    // TODO: suppress/ignore change signals from m_presentationControl
    switch( presentation )
    {
    case Sound:
        m_presentationControl->setPlaySound( presentations & Sound );
        break;
    case PassivePopup:
        m_presentationControl->setShowPassivePopup( presentations & PassivePopup );
        break;
    case PanelButton:
        m_presentationControl->setMarkButton( presentations & PanelButton );
        break;
    }

    m_presentationControl->blockSignals( false );

    emit changed( true );
}

void EventGroupListPresentationControl::onPresentationControlChange( int presentation, bool on )
{
    EventListViewItem *item = static_cast<EventListViewItem*>( m_eventListView->currentItem() );
    if( item )
    {
        item->event()->setPresentation( presentation, on );
        item->setSymbols();

        m_currentEventGroup->setDirty();

        emit changed( true );
   }
}

EventGroupListPresentationControl::~EventGroupListPresentationControl() {}

}

#include "eventgrouplistpresentationcontrol.moc"
