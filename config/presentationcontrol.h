/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PRESENTATIONCONTROL_H
#define PRESENTATIONCONTROL_H


// Qt
#include <qwidget.h>

class QPushButton;
class QCheckBox;
class KURLRequester;


class PresentationControl : public QWidget
{
    Q_OBJECT

public:
    PresentationControl( QWidget *parent = 0, const char *name = 0 );
    virtual ~PresentationControl();

public:
   void setSoundFileURL( const QString &url );
   void setPlaySound( bool on );
   void setShowPassivePopup( bool on );
   void setMarkButton( bool on );

public:
    bool playSound() const;
    QString soundFileURL() const;
    bool showPassivePopup() const;
    bool markButton() const;

signals:
    void presentationChanged( int presentation, bool on );
    void changed( bool );

private slots:
    void onPlaySoundStateChange( bool on );
    void onSoundFileURLChange( const QString &url );
    void onShowPassivePopupStateChange( bool on );
    void onMarkButtonStateChange( bool on );

    void initSoundFileDialog( KURLRequester *requester );
    void playSoundFile();

protected:
    QCheckBox *m_playSoundCheckBox;
    QPushButton *m_playButton;
    KURLRequester *m_soundFileURLRequester;
    QCheckBox *m_showPassivePopupCheckBox;
    QCheckBox *m_markButtonCheckBox;
};

#endif
