/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qlayout.h>
#include <qcheckbox.h>
#include <qpushbutton.h>
#include <qtooltip.h>
// KDE
#include <kiconloader.h>
#include <kaudioplayer.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kdialog.h>
#include <kfiledialog.h>
#include <kurlrequester.h>
#include <kio/netaccess.h>
#include <kmessagebox.h>
// module
#include "presentation.h"
#include "events.h"
#include "presentationcontrol.h"


PresentationControl::PresentationControl( QWidget *parent, const char *name )
 : QWidget( parent, name )
{
    QVBoxLayout *topLayout = new QVBoxLayout( this, 0, KDialog::spacingHint() );

    // play sound
    QHBoxLayout *playSoundLayout = new QHBoxLayout( KDialog::spacingHint() );

    m_playSoundCheckBox = new QCheckBox( i18n("Play a &sound:"), this );
        connect( m_playSoundCheckBox, SIGNAL(toggled( bool )),
                 SLOT(onPlaySoundStateChange( bool )) );
    m_playButton = new QPushButton( this );
        m_playButton->setEnabled( false );
        m_playButton->setIconSet( SmallIconSet("player_play") );
        QToolTip::add( m_playButton, i18n("Test the Sound") );
        connect( m_playButton, SIGNAL(clicked()), SLOT(playSoundFile()) );
    m_soundFileURLRequester = new KURLRequester( this );
        m_soundFileURLRequester->setEnabled( false );
        connect( m_soundFileURLRequester, SIGNAL(textChanged( const QString& )),
                 SLOT(onSoundFileURLChange( const QString& )) );
        connect( m_soundFileURLRequester, SIGNAL(openFileDialog( KURLRequester * )),
                 SLOT(initSoundFileDialog( KURLRequester * )) );

    playSoundLayout->addWidget( m_playSoundCheckBox );
    playSoundLayout->addWidget( m_playButton );
    playSoundLayout->addWidget( m_soundFileURLRequester );

    // show passive popup
    m_showPassivePopupCheckBox = new QCheckBox( i18n("Show a &message in a passive popup window"), this );
        connect( m_showPassivePopupCheckBox, SIGNAL(toggled( bool )),
                 SLOT(onShowPassivePopupStateChange( bool )) );

    // mark entry
    m_markButtonCheckBox = new QCheckBox( i18n("Mark applet button"), this );
        connect( m_markButtonCheckBox, SIGNAL(toggled( bool )),
                 SLOT(onMarkButtonStateChange( bool )) );

    topLayout->addLayout( playSoundLayout );
    topLayout->addWidget( m_showPassivePopupCheckBox );
    topLayout->addWidget( m_markButtonCheckBox );
}

bool PresentationControl::playSound() const
{
    return m_playSoundCheckBox->isChecked();
}

QString PresentationControl::soundFileURL() const
{
    return m_soundFileURLRequester->url();
}

bool PresentationControl::showPassivePopup() const
{
    return m_showPassivePopupCheckBox->isChecked();
}

bool PresentationControl::markButton() const
{
    return m_markButtonCheckBox->isChecked();
}

void PresentationControl::setSoundFileURL( const QString &url )
{
    m_soundFileURLRequester->setURL( url );

    m_playButton->setEnabled( m_playSoundCheckBox->isChecked() && !url.isEmpty() );
}

void PresentationControl::setPlaySound( bool on )
{
    m_playSoundCheckBox->setChecked( on );
    m_soundFileURLRequester->setEnabled( on );
    m_playButton->setEnabled( on && !m_soundFileURLRequester->url().isEmpty() );
}

void PresentationControl::setShowPassivePopup( bool on )
{
    m_showPassivePopupCheckBox->setChecked( on );
}

void PresentationControl::setMarkButton( bool on )
{
    m_markButtonCheckBox->setChecked( on );
}

void PresentationControl::onPlaySoundStateChange( bool on )
{
    m_soundFileURLRequester->setEnabled( on );
    m_playButton->setEnabled( on && !m_soundFileURLRequester->url().isEmpty() );

    emit presentationChanged( Khalkhi::Sound, on );
    emit changed( true );
}

void PresentationControl::onShowPassivePopupStateChange( bool on )
{
    emit presentationChanged( Khalkhi::PassivePopup, on );
    emit changed( true );
}

void PresentationControl::onMarkButtonStateChange( bool on )
{
    emit presentationChanged( Khalkhi::PanelButton, on );
    emit changed( true );
}


void PresentationControl::onSoundFileURLChange( const QString &url )
{
    if( signalsBlocked() )
        return;

    m_playButton->setEnabled( !url.isEmpty() );

    emit changed( true );
}

void PresentationControl::playSoundFile()
{
    const QString soundFileURL = m_soundFileURLRequester->url();

    // relative url?
    if( !KIO::NetAccess::exists(soundFileURL,true,0) )
    {
        bool soundFound = false;

        // find the first "sound"-resource that contains files
        const QStringList soundDirs = KGlobal::dirs()->resourceDirs( "sound" );

        QDir dir;
        dir.setFilter( QDir::Files | QDir::Readable );
        QStringList::ConstIterator endIt = soundDirs.end();
        for( QStringList::ConstIterator it = soundDirs.begin(); it!=endIt; ++it )
        {
            dir = *it;
            if( dir.isReadable() && dir.count() > 2
                && KIO::NetAccess::exists( *it + soundFileURL, true, 0 ) )
            {
                soundFound = true;
                break;
            }
        }
        if( !soundFound )
        {
            KMessageBox::sorry( this, i18n("The specified file does not exist.") );
            return;
        }
    }

    KAudioPlayer::play( soundFileURL );
}

void PresentationControl::initSoundFileDialog( KURLRequester *requester )
{
    // no further init needed, so disconnect
    requester->disconnect( SIGNAL(openFileDialog( KURLRequester * )),
                           this, SLOT(initSoundFileDialog( KURLRequester * )) );

    KFileDialog *fileDialog = requester->fileDialog();
    fileDialog->setCaption( i18n("Select Sound File") );
    QStringList filters;
    filters << "audio/x-wav" 
            << "audio/mpeg"
            << "application/ogg"
            << "audio/x-adpcm";
    fileDialog->setMimeFilter( filters );

    // find the first "sound"-resource that contains some files
    const QStringList soundDirs = KGlobal::dirs()->resourceDirs( "sound" );

    QDir dir;
    dir.setFilter( QDir::Files | QDir::Readable );
    QStringList::ConstIterator endIt = soundDirs.end();
    for( QStringList::ConstIterator it = soundDirs.begin(); it != endIt; ++it )
    {
        dir = *it;
        if( dir.isReadable() && dir.count() > 2 )
        {
            KURL soundURL;
            soundURL.setPath( *it );
            fileDialog->setURL( soundURL );
            break;
        }
    }
}

PresentationControl::~PresentationControl() {}


#include "presentationcontrol.moc"
