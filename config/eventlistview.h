/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation;

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef EVENTLISTVIEW_H
#define EVENTLISTVIEW_H

// Qt
#include <qtooltip.h>
#include <qpixmap.h>
// KDE
#include <klistview.h>
// applet
#include "events.h"


class PresentationToolTip : public QToolTip
{
public:
    PresentationToolTip( QHeader *header );
    virtual ~PresentationToolTip();

protected:
    virtual void maybeTip ( const QPoint &point );

private:
    QString m_tips[6];
};


namespace Khalkhi {

enum
{
    SoundColumn   = 0,
    MessageColumn = 1,
    PanelColumn   = 2,
    NameColumn    = 3
};

static const int PresentationCount = 3;


class EventListView : public KListView
{
    friend class EventListViewItem;

    Q_OBJECT

public:
    EventListView( QWidget* parent = 0, const char* name = 0 );
    virtual ~EventListView();

public:
    void setEventList( const EventList &events );

public slots:
    void updateItem();

signals:
    void presentationToggled( Event *event, int presentation );

private slots:
    void onItemClicked( QListViewItem *item, const QPoint &point, int column );

private:
    PresentationToolTip *m_toolTip;

    QPixmap m_presentationSymbols[PresentationCount];
};


class EventListViewItem : public QListViewItem
{
    friend class EventListView;

public:
    EventListViewItem( QListView *view, Event *event );

public:
    virtual int compare( QListViewItem *otherItem, int col, bool ascending ) const;

public:
    Event *event();
    void setSymbols();

private:
    Event *m_event;
};

inline Event *EventListViewItem::event() { return m_event; }

}

#endif
