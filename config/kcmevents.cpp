/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// Qt
#include <qlayout.h>
// KDE
#include <kstandarddirs.h>
// module
#include "events.h"
#include "eventgrouplistpresentationcontrol.h"
#include "kcmevents.h"


KEventsControlModule::KEventsControlModule( QWidget *parent, const char *name, const QStringList & )
 : KCModule( parent, name )
{
    QVBoxLayout *layout = new QVBoxLayout( this );

    m_presentationControl = new Khalkhi::EventGroupListPresentationControl( this );
    layout->addWidget( m_presentationControl );

    connect( m_presentationControl, SIGNAL(changed( bool )), SIGNAL(changed( bool )) );

    QStringList eventFilePathes =
        KGlobal::dirs()->findAllResources( "data", "khalkhi/services/*/eventsrc", false, true );

    Khalkhi::EventGroupList eventGroupList;

    QStringList::ConstIterator it = eventFilePathes.begin();
    for( ; it != eventFilePathes.end(); ++it )
    {
        // get relative path, which starts at the fourth slash
        const QString &filePath = *it;
        int slash = -1;
        for( int i=0; i<4; ++i )
        {
            slash = filePath.findRev( '/', slash ) - 1;
            if( slash < 0 )
                break;
        }
        if( slash < 0 )
            continue;

        eventGroupList.append( new Khalkhi::EventGroup(filePath.mid( slash+2 )) );
    }

    eventGroupList.sort();
    m_presentationControl->setEventGroups( eventGroupList );
}


void KEventsControlModule::load()
{
    m_presentationControl->reload();
}

void KEventsControlModule::save()
{
    m_presentationControl->save();
}

void KEventsControlModule::defaults()
{
    m_presentationControl->setToDefaults();
}


#include "kcmevents.moc"


// KDE
#include <kparts/genericfactory.h>

typedef KGenericFactory<KEventsControlModule, QWidget> KEventsControlModuleFactory;
K_EXPORT_COMPONENT_FACTORY( kcm_khalkhiapplet_events, KEventsControlModuleFactory("kcmevents") )
