/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// Qt
#include <qstyle.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
// KDE
#include <kcombobox.h>
#include <kicondialog.h>
#include <kiconloader.h>
#include <klocale.h>
// module
#include "personlistconfigdialog.h"


static const int IconPixelSize = 48;
static const int IconBorderPixelSize = 2;

PersonListConfigDialog::PersonListConfigDialog( const QString &Name, const KABC::Field::List &Fs, QWidget *Parent )
 : KDialogBase( Parent, 0, true, i18n("Configure List Button for %1").arg(Name), Ok|Cancel, Ok, true ),
   Fields( Fs )
{
    // base 
    QWidget *Page = new QWidget( this );
    setMainWidget( Page );
    QVBoxLayout *PageLayout = new QVBoxLayout( Page, 0, spacingHint() );

    QHBoxLayout *ItemLayout = new QHBoxLayout( PageLayout, spacingHint() );
    IconButton = new KIconButton( Page );
    int ButtonSize = IconPixelSize + 2*IconBorderPixelSize
                     + 2 * IconButton->style().pixelMetric( QStyle::PM_ButtonMargin );
    IconButton->setFixedSize( ButtonSize, ButtonSize );
    IconButton->setIconSize( IconPixelSize );
    IconButton->setStrictIconSize( false );

    ItemLayout->addWidget( IconButton );
    ItemLayout->addWidget( new QLabel(Name,Page) );
    ItemLayout->addStretch( 10 );

    QGroupBox *SettingsBox = new QGroupBox( 0, Qt::Vertical, i18n("List Display"), Page );
    SettingsBox->layout()->setSpacing( spacingHint() );
    QGridLayout *SettingsLayout = new QGridLayout( SettingsBox->layout() );
    SettingsLayout->setSpacing( spacingHint() );

        QLabel *Label = new QLabel( i18n("Name"), SettingsBox );
        NameComboBox = new KComboBox( false, SettingsBox );
        SettingsLayout->addWidget( Label, 0, 0 );
        SettingsLayout->addWidget( NameComboBox, 0, 1 );

    QGroupBox *SortingBox = new QGroupBox( 0, Qt::Vertical, i18n("List Sorting"), Page );
    SortingBox->layout()->setSpacing( spacingHint() );
    QGridLayout *SortingLayout = new QGridLayout( SortingBox->layout() );
    SortingLayout->setSpacing( spacingHint() );

        Label = new QLabel( i18n("Criterion"), SortingBox );
        SortingCriterionComboBox = new KComboBox( false, SortingBox );
        SortingLayout->addWidget( Label, 0, 0 );
        SortingLayout->addWidget( SortingCriterionComboBox, 0, 1 );

        Label = new QLabel( i18n("Order"), SortingBox );
        SortingOrderComboBox = new KComboBox( false, SortingBox );
        SortingLayout->addWidget( Label, 1, 0 );
        SortingLayout->addWidget( SortingOrderComboBox, 1, 1 );

    QGroupBox *GroupServicesBox = new QGroupBox( 0, Qt::Vertical, i18n("Actions"), Page );
    GroupServicesBox->layout()->setSpacing( spacingHint() );
    QGridLayout *GroupServicesLayout = new QGridLayout( GroupServicesBox->layout() );
    GroupServicesLayout->setSpacing( spacingHint() );

        GroupServicesCheckBox = new QCheckBox( i18n("Offer actions for the whole list"), GroupServicesBox );
        GroupServicesLayout->addWidget( GroupServicesCheckBox, 0, 0 );

    PageLayout->addWidget( SettingsBox );
    PageLayout->addWidget( SortingBox );
    PageLayout->addWidget( GroupServicesBox );
    PageLayout->addStretch( 10 );

    QStringList NameIds;
    NameIds << i18n("Formatted Name") << i18n("Nickname") << i18n("Given Name") << i18n("Family Name");
    NameComboBox->insertStringList( NameIds );

    KABC::Field::List::ConstIterator it;
    for( it = Fields.begin(); it != Fields.end(); ++it )
        SortingCriterionComboBox->insertItem( (*it)->label() );

    SortingOrderComboBox->insertItem( i18n("Ascending") );
    SortingOrderComboBox->insertItem( i18n("Descending") );

}


QString PersonListConfigDialog::iconName() const
{
    return IconButton->icon();
}

KNameType PersonListConfigDialog::nameType() const
{
    return (KNameType)NameComboBox->currentItem();
}

KABC::Field* PersonListConfigDialog::sortField() const
{
    return Fields[ SortingCriterionComboBox->currentItem() ];
}

Qt::SortOrder PersonListConfigDialog::sortOrder() const
{
    return (SortingOrderComboBox->currentItem()==0) ? Ascending : Descending;
}

bool PersonListConfigDialog::groupServices() const
{
    return GroupServicesCheckBox->isChecked();
}

void PersonListConfigDialog::setIconName( const QString &IconName )
{
    IconButton->setIcon( IconName );
}

void PersonListConfigDialog::setNameType( KNameType NameType )
{
    NameComboBox->setCurrentItem( NameType );
}

void PersonListConfigDialog::setSortField( KABC::Field *SortField )
{
    if( SortField )
    SortingCriterionComboBox->setCurrentText( SortField->label() );
}

void PersonListConfigDialog::setSortOrder( Qt::SortOrder Order )
{
    SortingOrderComboBox->setCurrentItem( Order == Ascending ? 0 : 1 );
}

void PersonListConfigDialog::setGroupServices( bool GroupServices )
{
    GroupServicesCheckBox->setChecked( GroupServices );
}
PersonListConfigDialog::~PersonListConfigDialog()
{}
