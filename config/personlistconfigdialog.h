/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PERSONLISTCONFIGDIALOG_H
#define PERSONLISTCONFIGDIALOG_H


// KDE
#include <kdialogbase.h>
#include <kabc/field.h>
// applet
#include "personlistmenufiller.h"

class QCheckBox;
class KComboBox;
class KIconButton;


class PersonListConfigDialog : public KDialogBase
{
public:
    PersonListConfigDialog( const QString &Name, const KABC::Field::List &Fs, QWidget *Parent = 0 );
    virtual ~PersonListConfigDialog();

public:
    KNameType nameType() const;
    QString iconName() const;
    KABC::Field* sortField() const;
    Qt::SortOrder sortOrder() const;
    bool groupServices() const;

public:
    void setIconName( const QString &IconName );
    void setNameType( KNameType NameType );
    void setSortField( KABC::Field *F );
    void setSortOrder( Qt::SortOrder Order );
    void setGroupServices( bool GroupServices );

protected:
    const KABC::Field::List Fields;

    KIconButton *IconButton;

    KComboBox *NameComboBox;

    KComboBox *SortingCriterionComboBox;
    KComboBox *SortingOrderComboBox;

    QCheckBox *GroupServicesCheckBox;
};

#endif
