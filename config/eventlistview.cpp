/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation;

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qheader.h>
// KDE
#include <klocale.h>
#include <kiconloader.h>
// applet
#include "presentation.h"
#include "eventlistview.h"


PresentationToolTip::PresentationToolTip( QHeader *header )
 : QToolTip( header )
{
    m_tips[Khalkhi::MessageColumn] = i18n( "Display a message popup" );
    m_tips[Khalkhi::SoundColumn]   = i18n( "Play a sound" );
    m_tips[Khalkhi::PanelColumn]   = i18n( "Flash the panel entry" );
}

void PresentationToolTip::maybeTip( const QPoint &point )
{
    QHeader *header = static_cast<QHeader*>( parentWidget() );

    int section = header->sectionAt( (header->orientation()==Horizontal) ? point.x() : point.y() );

    if( section >= 0 && section < Khalkhi::PresentationCount )
        tip( header->sectionRect(section), m_tips[section] );
}

PresentationToolTip::~PresentationToolTip() {}



static const int SymbolPadding = 3;

namespace Khalkhi {

EventListView::EventListView( QWidget *parent, const char *name )
 : KListView( parent, name )
{
    const QPixmap messageSymbol = SmallIcon( "info" );
    const QPixmap soundSymbol =   SmallIcon( "sound" );
    const QPixmap panelSymbol =   SmallIcon( "kicker" );

    m_presentationSymbols[MessageColumn] = messageSymbol;
    m_presentationSymbols[SoundColumn]   = soundSymbol;
    m_presentationSymbols[PanelColumn] = panelSymbol;

    const int SymbolColumnWidth = KIcon::SizeSmall + 2*SymbolPadding;

    QHeader *listHeader = header();

    addColumn( QString::null );
    listHeader->setResizeEnabled( false, SoundColumn );
    listHeader->setLabel( SoundColumn,   soundSymbol,   QString::null, SymbolColumnWidth );
    addColumn( QString::null );
    listHeader->setResizeEnabled( false, MessageColumn );
    listHeader->setLabel( MessageColumn, messageSymbol, QString::null, SymbolColumnWidth );
    addColumn( QString::null );
    listHeader->setResizeEnabled( false, PanelColumn );
    listHeader->setLabel( PanelColumn,   panelSymbol,   QString::null, SymbolColumnWidth );
    addColumn( i18n("Event") );

    setFullWidth( true );
    setAllColumnsShowFocus( true );

    m_toolTip = new PresentationToolTip( listHeader );

    connect( this, SIGNAL(clicked( QListViewItem *, const QPoint&, int)),
             SLOT( onItemClicked( QListViewItem *, const QPoint&, int )));
}

void EventListView::setEventList( const EventList &events )
{
    clear();

    EventListIterator it( events );
    for( ; it.current(); ++it )
        new EventListViewItem( this, it.current() );

    setSorting( NameColumn, true );
    sort();

    QListViewItem *item = firstChild();
    if( item )
        setSelected( item, true );
}


void EventListView::updateItem()
{
    EventListViewItem *item = static_cast<EventListViewItem*>( currentItem() );

    if( item )
        item->setSymbols();
}

void EventListView::onItemClicked( QListViewItem *i, const QPoint&, int column )
{
    EventListViewItem *item = static_cast<EventListViewItem*>( i );

    if( !item )
        return;

    Event *event = item->event();

    int presentation;
    switch( column )
    {
        case MessageColumn:
            presentation = PassivePopup;
            break;
        case SoundColumn:
            presentation = Sound;
            break;
        case PanelColumn:
            presentation = PanelButton;
            break;
        default:
            presentation = NoPresentation;
            break;
    }

    if( presentation != NoPresentation )
    {
        event->invertPresentation( presentation );
        item->setSymbols();
        emit presentationToggled( event, presentation );
    }
}

EventListView::~EventListView()
{
    delete m_toolTip;
}


EventListViewItem::EventListViewItem( QListView *view, Event *event )
 : QListViewItem( view ),
   m_event( event )
{
    setText( NameColumn, event->description() );
    setSymbols();
}

int EventListViewItem::compare( QListViewItem *oi, int column, bool ascending ) const
{
    EventListViewItem *otherItem = static_cast<EventListViewItem*>( oi );

    int presentation = NoPresentation;

    switch( column )
    {
    case NameColumn:
        // default sorting
        return QListViewItem::compare( oi, NameColumn, ascending );

    case MessageColumn:
        presentation = PassivePopup;
        break;
    case SoundColumn:
        presentation = Sound;
        break;
    case PanelColumn:
        presentation = PanelButton;
        break;
    }

    const bool hasPresentation = m_event->hasPresentation( presentation );
    const bool otherHasPresentation = otherItem->m_event->hasPresentation( presentation );

    int result =
        ( hasPresentation == otherHasPresentation ) ?
            // default sorting by event
            QListViewItem::compare( oi, NameColumn, true ) :
        hasPresentation ?
            -1 : 1;

    return result;
}

void EventListViewItem::setSymbols()
{
    EventListView *view = static_cast<EventListView*>( listView() );

    QPixmap noSymbol;

    bool showSymbol = m_event->hasPresentation( Sound ) && !m_event->soundFileURL().isEmpty();
    setPixmap( SoundColumn, showSymbol ? view->m_presentationSymbols[SoundColumn] : noSymbol );

    showSymbol = m_event->hasPresentation( PassivePopup );
    setPixmap( MessageColumn, showSymbol ? view->m_presentationSymbols[MessageColumn] : noSymbol );

    showSymbol = m_event->hasPresentation( PanelButton );
    setPixmap( PanelColumn, showSymbol ? view->m_presentationSymbols[PanelColumn] : noSymbol );
}

}

#include "eventlistview.moc"
