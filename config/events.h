/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KHALKHI_EVENTS_H
#define KHALKHI_EVENTS_H


// Qt
#include <qstring.h>


namespace Khalkhi {

class EventGroup;


class Event
{
public:
    explicit Event( const EventGroup *group );

public:
    void setName( const QString &name );
    void setDescription( const QString &description );
    void setConfigName( const QString &configName );
    void setPresentations( int presentations );
    void setPresentation( int presentation, bool on );
    void invertPresentation( int presentation );
    void setSoundFileURL( const QString &name );

public:
    QString name() const;
    QString description() const;
    QString configName() const;

    int presentations() const;
    bool hasPresentation( int P ) const;
    QString soundFileURL() const;

private:
    int m_presentations;
    QString m_soundFileURL;

    QString m_name;
    QString m_description;
    QString m_configName;

    const EventGroup *m_group;
};

inline Event::Event( const EventGroup *group ) : m_presentations( 0 ), m_group( group ) {}

inline void Event::setName( const QString &name )               { m_name = name; }
inline void Event::setDescription( const QString &description ) { m_description = description; }
inline void Event::setConfigName( const QString &configName )   { m_configName = configName; }
inline void Event::setPresentations( int presentations )        { m_presentations = presentations; }
inline void Event::invertPresentation( int presentation )       { m_presentations ^= presentation; }
inline void Event::setPresentation( int presentation, bool on )
{ if( on ) m_presentations |= presentation; else m_presentations &= ~presentation; }

inline void Event::setSoundFileURL( const QString &name )      { m_soundFileURL = name; }

inline QString Event::name()          const { return m_name; }
inline QString Event::description()   const { return m_description; }
inline QString Event::configName()    const { return m_configName; }
inline int Event::presentations()     const { return m_presentations; }
inline bool Event::hasPresentation( int presentation ) const { return m_presentations & presentation; }
inline QString Event::soundFileURL() const { return m_soundFileURL; }


typedef QPtrList<Event> EventList;
typedef QPtrListIterator<Event> EventListIterator;


/**
 * with lazy loading
 */
class EventGroup
{
public:
    EventGroup( const QString &path );
    ~EventGroup();

public:
    QString name() const;
    QString description() const;
    QString icon() const;

public:
    const EventList &eventList();
    void reload( bool revertToDefaults = false );
    void sync();
    void setDirty();

private:
    QString m_name;
    QString m_icon;
    QString m_description;
    EventList *m_events;

    // definition of events
    KConfig *m_eventConfig;
    // configuration of presentations
    KConfig *m_presentationConfig;

    bool m_dirty;
};

inline  QString EventGroup::name()        const { return m_name; }
inline  QString EventGroup::description() const { return m_description; }
inline  QString EventGroup::icon()        const { return m_icon; }
inline  void EventGroup::setDirty() { m_dirty = true; }


class EventGroupList : public QPtrList<EventGroup>
{
public:
    virtual int compareItems( QPtrCollection::Item item1, QPtrCollection::Item item2 );
};

typedef QPtrListIterator<EventGroup> EventGroupListIterator;

}

#endif
