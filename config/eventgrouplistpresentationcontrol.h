/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef EVENTGROUPLISTPRESENTATIONCONTROL_H
#define EVENTGROUPLISTPRESENTATIONCONTROL_H


// Qt
#include <qwidget.h>
// applet
#include "events.h"

class QListViewItem;
class KComboBox;
class PresentationControl;

namespace Khalkhi {

class EventListView;


class EventGroupListPresentationControl : public QWidget
{
    Q_OBJECT

public:
    EventGroupListPresentationControl( QWidget *parent = 0, const char *name = 0 );
    virtual ~EventGroupListPresentationControl();

public:
    void setEventGroups( const EventGroupList &eventGroupList );
    void save();
    void reload();
    void setToDefaults();

public slots:
    void onEventGroupActivated( const QString &description );
    void onEventChange( QListViewItem *item );
    void onPresentationToggle( Event *event, int presentation );
    void onPresentationControlChange( int presentation, bool on );

signals:
    void changed( bool );

protected:
    EventGroupList m_eventGroupList;

    EventGroup *m_currentEventGroup;

    KComboBox *m_eventGroupCombo;
    EventListView *m_eventListView;
    PresentationControl *m_presentationControl;
};

}

#endif
