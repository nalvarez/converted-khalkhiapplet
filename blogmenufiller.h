/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef BLOGMENUFILLER_H
#define BLOGMENUFILLER_H


// kde specific
#include <kurl.h>
#include <kabc/addressee.h>
// librss specific
#include "librss/loader.h"
#include "librss/document.h"
// menu specific
#include "menufiller.h"

using namespace RSS;

class BlogMenuFiller : public QObject, public MenuFiller
{
  Q_OBJECT
public:
    static void createMenuEntry( const KABC::Addressee Contact, QPopupMenu *Menu );

public:
    BlogMenuFiller( const KURL U );

public: // MenuFiller API
    virtual int fill( QPopupMenu *Menu );

protected slots:
    void browse( int Id ) const;
    void loadingComplete( Loader*, Document Document, Status Status );

protected:
    /** URL of the blog feed */
    const KURL FeedURL;
    RSS::Article::List ArticleList;
    /**  */
    QPopupMenu *CurrentMenu;
};

#endif
