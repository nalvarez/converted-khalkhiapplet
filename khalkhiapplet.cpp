/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qevent.h>
#include <qpopupmenu.h>
#include <qstringlist.h>
// KDE
#include <kaboutapplication.h>
#include <kapplication.h>
#include <klocale.h>
#include <kiconloader.h>
#include <kmessagebox.h>
#include <kstdguiitem.h>
#include <krun.h>
#include <kcmultidialog.h>
// Khalkhi
#include <allpropertiesglobaldataactionservicemenufiller.h>
#include <listallpropertiesglobaldataactionservicemenufiller.h>
//
#include <personselectdialog.h>
#include <kcategoryselectdialog.h>
// applet
#include "about.h"
#include "menubutton.h"
#include "personmenubuttonfiller.h"
#include "personlistmenubuttonfiller.h"
// #include "personconfigdialog.h"
#include "personlistconfigdialog.h"
#include "addresseelistspan.h"
#include "khalkhiapplet.h"


const char * const KhalkhiApplet::AppletName = "khalkhiapplet";

static const char ButtonsGroupId[] = "Buttons";
static const char ButtonMaskGroupId[] = "Button%1";
static const char CategoryId[] = "Category";
static const char IconNameId[] = "IconName";
static const char DisplayNameId[] = "DisplayName";
static const char SortFieldId[] = "SortField";
static const char SortOrderId[] = "SortOrder";
static const char GroupServicesId[] = "GroupServices";
static const char UIDId[] = "UID";
static const char ButtonTypeId[] = "ButtonType";
static const char NoOfButtonsId[] ="NoOfButtons";


KhalkhiApplet::KhalkhiApplet( const QString& ConfigFile, QWidget *Parent, const char *Name )
: KPanelApplet( ConfigFile, Normal, About|Preferences, Parent, Name ),
  Book( 0 ),
  DropServices( new Khalkhi::AllPropertiesGlobalDataActionServiceMenuFiller() ),
  ListDropServices( new Khalkhi::ListAllPropertiesGlobalDataActionServiceMenuFiller() )
{
    if( !Parent )
        setBackgroundMode( X11ParentRelative );

    setAcceptDrops( true );

    Book = StdAddressBook::self(); // TODO: care for asynchronoues loading (true);

    // TODO: make sure slotClear can be also called if the menu is open...
    connect( Book, SIGNAL(addressBookChanged(AddressBook*)), SLOT(onAddressBookChange() ));
    connect( Khalkhi::Services::self(), SIGNAL(changed()), SLOT(onPersonsChange()) );

    fillButtons();
}


void KhalkhiApplet::about()
{
    KhalkhiAppletAboutData AboutData;
    KAboutApplication Dialog( &AboutData );
    Dialog.exec();
}


void KhalkhiApplet::launchAddressbook()
{
    KApplication::startServiceByDesktopName( "kaddressbook" );
}


void KhalkhiApplet::preferences()
{
    KCMultiDialog configDialog( KCMultiDialog::IconList, i18n("Configure People applet"), 0, 0, true );

    configDialog.addModule( "kcmkhalkhi.desktop" );
    configDialog.addModule( "khalkhiapplet_events.desktop" );

    configDialog.exec();
}

int KhalkhiApplet::widthForHeight( int Height ) const
{
    return ButtonList.count() * Height;
}

int KhalkhiApplet::heightForWidth( int Width ) const
{
    return ButtonList.count() * Width;
}

void KhalkhiApplet::positionChange( Position P )
{
    for( QValueList<MenuButton*>::Iterator it = ButtonList.begin(); it != ButtonList.end(); ++it )
        (*it)->setPanelPosition( P );

    arrangeButtons();
}


MenuButton* KhalkhiApplet::buttonAt( const QPoint Pos ) const
{
    MenuButton* Result = 0;

    for( QValueList<MenuButton*>::ConstIterator it = ButtonList.begin(); it != ButtonList.end(); ++it )
        if( (*it)->geometry().contains(Pos) )
        {
            Result = *it;
            break;
        }

    return Result;
}

void KhalkhiApplet::arrangeButtons()
{
    int XOffset;
    int YOffset;

    if( orientation() == Vertical )
    {
        PixmapSize = width();
        XOffset = 0;
        YOffset = PixmapSize;
    }
    else
    {
        PixmapSize = height();
        XOffset = PixmapSize;
        YOffset = 0;
    }

    int i = 0;
    for( QValueList<MenuButton*>::Iterator it = ButtonList.begin(); it != ButtonList.end(); ++it,++i )
    {
        (*it)->resize( PixmapSize, PixmapSize );
        (*it)->move( i*XOffset, i*YOffset );
    }

    updateGeometry();
    emit updateLayout();
}


void KhalkhiApplet::mousePressEvent( QMouseEvent *ME )
{
    if( ME->button() == RightButton )
    {
        if (!kapp->authorizeKAction("kicker_rmb"))
            return;

        MenuButton* Button = buttonAt( ME->pos() );

        QPopupMenu Menu;

        Menu.insertItem( SmallIconSet("kaddressbook"), i18n("Address book"), 1 ); // KDE 4 icon name: office-address-book
        Menu.insertSeparator();
        if( Button )
        {
            const unsigned int CountBefore = Menu.count();
            PersonListMenuButtonFiller *Filler = dynamic_cast<PersonListMenuButtonFiller*>( Button->filler() );
            if( Filler )
                Menu.insertItem( SmallIcon("configure"), i18n("&Configure \"%1\" button...").arg(Button->titel()), 5 );
            if( ButtonList.size() > 1 )
                Menu.insertItem( SmallIcon("remove"), i18n("Remove \"%1\" button").arg(Button->titel()), 6 ); // KDE 4 icon name: list-remove
            if( CountBefore < Menu.count() )
                Menu.insertSeparator();
        }
        QPopupMenu AddMenu;
            AddMenu.insertItem( SmallIcon("identity"),     i18n("New Person Button..."), 2 ); // KDE 4 name: contact-new
            AddMenu.insertItem( SmallIcon("kuser"),        i18n("New Category Button..."), 3 ); // KDE 4 name: user-group-new
            AddMenu.insertItem( SmallIcon("kaddressbook"), i18n("New All Persons Button"), 4 ); // KDE 4 name: address-book-new
        Menu.insertItem( i18n("Add"), &AddMenu );
        Menu.insertSeparator();
        Menu.insertItem( SmallIcon("configure"), i18n("&Configure People Applet..."), 7 );

        int Choice = Menu.exec( mapToGlobal(ME->pos()) );

        switch( Choice )
        {
        case 1 : launchAddressbook(); break;
        case 2 : addButton( PersonButton ); break;
        case 3 : addButton( CategoryButton ); break;
        case 4 : addButton( AllButton ); break;
        case 5 : configureButton( Button ); break;
        case 6 : removeButton( Button ); break;
        case 7 : preferences(); break;
        }
    }
}


void KhalkhiApplet::configureButton( MenuButton *Button )
{
#if 0
    if( PersonMenuButtonFiller *Filler = dynamic_cast<PersonMenuButtonFiller*>( Button->filler() ) )
     {
        PersonConfigDialog *Dialog = new PersonConfigDialog( Button->titel(), this );
        if( Dialog->exec() )
        {
            writeConfig();
        }
        delete Dialog;
    }
    else
#endif
    if( PersonListMenuButtonFiller *Filler = dynamic_cast<PersonListMenuButtonFiller*>( Button->filler() ) )
    {
        PersonListConfigDialog *Dialog = new PersonListConfigDialog(
            Button->titel(), Book->fields(), this );
        Dialog->setIconName( Filler->iconName() );
        Dialog->setNameType( Filler->nameType() );
        Dialog->setSortField( Filler->sortField() );
        Dialog->setSortOrder( Filler->sortOrder() );
        Dialog->setGroupServices( Filler->groupServices() );
        if( Dialog->exec() )
        {
            Filler->setIconName( Dialog->iconName() );
            Filler->setNameType( Dialog->nameType() );
            Filler->setSortField( Dialog->sortField() );
            Filler->setSortOrder( Dialog->sortOrder() );
            Filler->setGroupServices( Dialog->groupServices() );
            Filler->fill( Button );
            writeConfig();
        }
        delete Dialog;
    }
}


void KhalkhiApplet::resizeEvent( QResizeEvent */*RE*/ )
{
     arrangeButtons();
}


void KhalkhiApplet::writeConfig()
{
    KConfig* Config = config();

    int i = 0;
    for( QValueList<MenuButton*>::Iterator it = ButtonList.begin(); it != ButtonList.end(); ++it,++i )
    {
        Config->setGroup( QString::fromLatin1(ButtonMaskGroupId).arg(i) );

        ButtonType Type;

        MenuButtonFiller *Filler = (*it)->filler();
        PersonMenuButtonFiller *PersonFiller;
        PersonListMenuButtonFiller *PersonListFiller = dynamic_cast<PersonListMenuButtonFiller *>(Filler);
        if( PersonListFiller )
        {
            Type = PersonListFiller->isCategory() ? CategoryButton : AllButton;
            if( Type == CategoryButton )
                Config->writeEntry( CategoryId, PersonListFiller->titel() );

            Config->writeEntry( IconNameId,      PersonListFiller->iconName() );
            Config->writeEntry( DisplayNameId,   PersonListFiller->nameType() );
            Config->writeEntry( SortFieldId,     PersonListFiller->sortField()->label() );
            Config->writeEntry( SortOrderId,     PersonListFiller->sortOrder() );
            Config->writeEntry( GroupServicesId, PersonListFiller->groupServices() );
        }
        else if( (PersonFiller = dynamic_cast<PersonMenuButtonFiller *>(Filler)) )
        {
            Type = PersonButton;
            Config->writeEntry( UIDId, PersonFiller->person().uid() );
        }
        else // should not happen!!!
        {
          --i; continue;
        }

        Config->writeEntry( ButtonTypeId, Type );
    }

    Config->setGroup( ButtonsGroupId );
    Config->writeEntry( NoOfButtonsId, i );
    Config->sync();
}

void KhalkhiApplet::fillButtons()
{
    KConfig *Config = config();
    Config->setGroup( ButtonsGroupId );
    int NoOfButtons = Config->readNumEntry( NoOfButtonsId );

    // None stored, perhaps due to fresh installation?
    if( NoOfButtons == 0 )
        addButton( AllButton );
    else
        for( int i=0; i<NoOfButtons; ++i )
        {
            Config->setGroup( QString::fromLatin1(ButtonMaskGroupId).arg(i) );
            const ButtonType Type = (ButtonType)Config->readNumEntry( ButtonTypeId, AllButton );

            if( Type == PersonButton )
                addPersonButton( Config->readEntry(UIDId) );
            else
            {
                const QString IconName = Config->readEntry( IconNameId, QString::fromLatin1("kaddressbook") );
                const KNameType NameType = (KNameType)Config->readNumEntry( DisplayNameId, DisplayFormattedName );
                const QString SortField = Config->readEntry( SortFieldId );
                const Qt::SortOrder SortOrder = (Qt::SortOrder)Config->readNumEntry( SortOrderId, Qt::Ascending );
                const bool GroupServices = (bool)Config->readNumEntry( GroupServicesId, 0 );

                QString Category = QString::null;
                if( Type == CategoryButton )
                    Category = Config->readEntry( CategoryId );
                addListButton( Category, IconName, NameType, SortField, SortOrder, GroupServices );
            }
        }

    arrangeButtons();
}


void KhalkhiApplet::addButton( ButtonType Type )
{
    if( Type == PersonButton )
    {
        bool Ok;
        const KABC::Addressee Person =
            PersonSelectDialog::getPerson( i18n("Add button"), i18n("Select Person:"), Book, &Ok );

        if( !Ok )
            return;
        addPersonButton( Person.uid() );
    }
    else
    {
        const QString IconName = QString::fromLatin1( "kaddressbook" );
        const KNameType NameType = DisplayFormattedName;
        const QString SortField = QString::null;
        const Qt::SortOrder SortOrder = Qt::Ascending;
        const bool GroupServices = false;
        QString Category = QString::null;
        if( Type == CategoryButton )
        {
            bool Ok;
            Category =
                KCategorySelectDialog::getCategory( i18n("Add button"), i18n("Select Category:"), Book, &Ok );

            if( !Ok )
                return;
        }
        addListButton( Category, IconName, NameType, SortField, SortOrder, GroupServices );
    }

    writeConfig();
    arrangeButtons();
}


void KhalkhiApplet::addPersonButton( const QString &Uid  )
{
    const KABC::Addressee Person = Book->findByUid( Uid );
    if( !Person.isEmpty() )
        appendButton( new MenuButton(new PersonMenuButtonFiller( Person ),this) );
}

static KABC::Field *findField( KABC::AddressBook *Book, const QString &Label )
{
    const KABC::Field::List Fields = Book->fields();

    KABC::Field *Result = Fields[0];

    KABC::Field::List::ConstIterator it;
    for( it = Fields.begin(); it != Fields.end(); ++it )
        if( Label == (*it)->label() )
        {
            Result = *it;
            break;
        }

    return Result;
}

void KhalkhiApplet::addListButton( const QString &CategoryName, const QString &IconName, KNameType NameType,
                                        const QString &FieldName, Qt::SortOrder SortOrder, bool GroupServices )
{
    const bool IsCategory = !CategoryName.isEmpty();

    KABC::AddresseeList AddressList = IsCategory ?
        Book->findByCategory( CategoryName ) :
        Book->allAddressees();

    const QString Titel = IsCategory ? CategoryName : i18n( "All persons" );

    PersonListMenuButtonFiller *Filler =
        new PersonListMenuButtonFiller( AddressList, IconName, NameType, Titel, IsCategory );
    Filler->setSortField( findField(Book,FieldName) );
    Filler->setSortOrder( SortOrder );
    Filler->setGroupServices( GroupServices );

    appendButton( new MenuButton(Filler,this) );
}

void KhalkhiApplet::appendButton( MenuButton *Button )
{
    Button->setPanelPosition( position() );
    Button->show(); //TODO: vielleicht erst nach arrangeButtons showen?
    ButtonList.append( Button );
}


void KhalkhiApplet::removeButton( MenuButton *Button )
{
    ButtonList.remove( Button );
    delete Button;

    if( ButtonList.isEmpty() )
        addButton( AllButton );

    writeConfig();
    arrangeButtons();
}

void KhalkhiApplet::onAddressBookChange()
{
    Book = StdAddressBook::self();

    // remove old
    QValueList<MenuButton*>::ConstIterator it;
    QValueList<MenuButton*>::ConstIterator itEnd = ButtonList.end();
    for( it = ButtonList.begin(); it != itEnd; ++it )
        delete (*it);
    ButtonList.clear();

    fillButtons();
}

void KhalkhiApplet::onPersonsChange()
{
    // remove old
    QValueList<MenuButton*>::ConstIterator it;
    QValueList<MenuButton*>::ConstIterator itEnd = ButtonList.end();
    for( it = ButtonList.begin(); it != itEnd; ++it )
        delete (*it);
    ButtonList.clear();

    fillButtons();
}


void KhalkhiApplet::dragMoveEvent( QDragMoveEvent *Event )
{
    bool Dropable = false;
    MenuButton* Button = buttonAt( Event->pos() );
    if( Button )
    {
        if( PersonMenuButtonFiller *Filler = dynamic_cast<PersonMenuButtonFiller*>(Button->filler()) )
        {
            DropServices->set( Filler->person(), Event );
            Dropable = DropServices->serviceAvailableForData();
        }
        else if( PersonListMenuButtonFiller *Filler = dynamic_cast<PersonListMenuButtonFiller*>(Button->filler()) )
        {
            if( Filler->groupServices() )
            {
                ListDropServices->set( Filler->personList(), Event );
                Dropable = ListDropServices->serviceAvailableForData();
            }
        }
    }
    Event->accept( Dropable );
}

void KhalkhiApplet::dropEvent( QDropEvent *Event )
{
    MenuButton* Button = buttonAt( Event->pos() );
    if( Button )
    {
        if( PersonMenuButtonFiller *Filler = dynamic_cast<PersonMenuButtonFiller*>(Button->filler()) )
        {
            QPopupMenu *Menu = new QPopupMenu();

            DropServices->set( Filler->person(), Event );
            DropServices->fillMenu( Menu, 0 );
            Menu->insertSeparator();
            Menu->insertItem( SmallIcon("cancel"), i18n("&Cancel") ); // KDE 4 name: dialog-cancel

            Menu->exec( mapToGlobal(Event->pos()) );
            delete Menu;
        }
        else if( PersonListMenuButtonFiller *Filler = dynamic_cast<PersonListMenuButtonFiller*>(Button->filler()) )
        {
            if( Filler->groupServices() )
            {
                QPopupMenu *Menu = new QPopupMenu();

                ListDropServices->set( Filler->personList(), Event );
                ListDropServices->fillMenu( Menu, 0 );
                Menu->insertSeparator();
                Menu->insertItem( SmallIcon("cancel"), i18n("&Cancel") ); // KDE 4 name: dialog-cancel

                Menu->exec( mapToGlobal(Event->pos()) );
                delete Menu;
            }
        }
    }
}


KhalkhiApplet::~KhalkhiApplet()
{
    KGlobal::locale()->removeCatalogue( AppletName );
    delete DropServices;
    delete ListDropServices;
}

#include "khalkhiapplet.moc"
