/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// qt specific
#include <qpopupmenu.h>
// kde specific
#include <kiconloader.h>
#include <klocale.h>
// menu specific
#include "lazyfillmenu.h"
#include "personmenufiller.h"
#include "personlistmenufiller.h"


PersonListMenuFiller::PersonListMenuFiller( const AddresseeListSpan Span, KNameType NT,
                                            const QString &FirstToken, const QString &LastToken )
 : PersonsSpan( Span ), UpperFirstEntryToken( FirstToken ), UpperLastEntryToken( LastToken ), NameType( NT )
{
}

PersonListMenuFiller::PersonListMenuFiller( const AddresseeListSpan Span, KNameType NT )
 : PersonsSpan( Span ), NameType( NT )
{
    QString Dummy;
    // we only need and can cre
    if( Span.number() > 1 )
    {
        KABC::Addressee::List::ConstIterator it = PersonsSpan.begin();
        const QString &FirstName = nameForPerson(*it);
        const QString &SecondName = nameForPerson(*++it);
        createTokens( UpperFirstEntryToken, Dummy, FirstName, SecondName );

        it = PersonsSpan.end();
        const QString &LastName = nameForPerson(*--it);
        const QString &PreLastName = nameForPerson(*--it);
        createTokens( Dummy, UpperLastEntryToken, PreLastName, LastName );
    }
}

// TODO: calculate limit by screen size
static const int Limit = 30;

static const int MinChars = 3;
static const int MaxChars = 6;

void PersonListMenuFiller::createTokens( QString &PreviousToken, QString &NextToken, 
                                      const QString &Previous, const QString &Next )
{
    // finde gemeinsame Länge
    unsigned int Length = MaxChars;
    if( Previous.length() < Length )
        Length = Previous.length();
    if( Next.length() < Length )
        Length = Next.length();

    // suche Position des Unterschiedes
    unsigned int i = MinChars-1;
    for( ; i<Length; ++i )
        if( Previous[i] != Next[i] )
            break;

    // 
    ++i;
    PreviousToken = Previous.left( i ).stripWhiteSpace();
    NextToken =     Next.left( i ).stripWhiteSpace();
}

int PersonListMenuFiller::fill( QPopupMenu *Menu )
{
    if( PersonsSpan.number() > Limit )
    {
        int SizeSubMenus = PersonsSpan.number() / Limit + 1;

        KABC::Addressee::List::ConstIterator it = PersonsSpan.begin();
        QString NextFirstEntryToken = UpperFirstEntryToken;
        QString LastEntryToken;

        do
        {
            const AddresseeListSpan SubSpan( it, SizeSubMenus, PersonsSpan.end() );

            // create shorts
            QString FirstEntryToken = NextFirstEntryToken;
            if( it != PersonsSpan.end() )
                createTokens( LastEntryToken, NextFirstEntryToken,
                              nameForPerson(SubSpan.last()), nameForPerson(*it) );
            else
                LastEntryToken = UpperLastEntryToken;

            MenuFiller *Filler = new PersonListMenuFiller( SubSpan, NameType, FirstEntryToken, LastEntryToken );
            LazyFillMenu *SubMenu = new LazyFillMenu( Filler, false, Menu );

            Menu->insertItem( SmallIconSet("contents"), escapeAmpersand(FirstEntryToken+QString("...")+LastEntryToken), SubMenu );
        }
        while( it != PersonsSpan.end() );
    }
    else if( PersonsSpan.number() > 0 )
    {
        KABC::Addressee::List::ConstIterator it = PersonsSpan.begin();

        while( it != PersonsSpan.end() )
        {
            const KABC::Addressee &Person = *it++;
            PersonMenuFiller::createMenuEntry( Person, nameForPerson(Person), Menu );
        }
    }
    else
    {
        int id = Menu->insertItem( i18n("No Entries"), 0 );
        Menu->setItemEnabled( id, false );
    }
    return 1;
}
